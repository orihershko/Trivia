#pragma once
#include "LoginManager.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"
#include "HighScoreTable.h"
#include "RoomManager.h"
#include "Game.h"
#include "GameManager.h"
class Game;
class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;

class RequestHandlerFactory
{
public:
	RequestHandlerFactory(LoginManager *loginManager, RoomManager *roomManager, HighScoreTable *ighScoreTable, GameManager *gameManager);
	~RequestHandlerFactory();

	LoginRequestHandler *createLoginRequestHandler();
	MenuRequestHandler *createMenuRequestHandler(LoggedUser user);
	RoomAdminRequestHandler *createRoomAdminRequestHandler(LoggedUser user, Room room, RoomManager &roomManager);
	RoomMemberRequestHandler *createRoomMemberRequestHandler(LoggedUser user, Room room, RoomManager &roomManager);
	GameRequestHandler *createGameRequestHandler(LoggedUser user, Room room);

private:
	LoginManager *_loginManager;
	RoomManager * _roomManager;
	GameManager * _gameManager;
	HighScoreTable * _highScoreTable;
};
