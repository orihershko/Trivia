﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace client_cs
{
    /// <summary>
    /// Interaction logic for errorWin.xaml
    /// </summary>
    public partial class errorWin : Window
    {
        public errorWin(string error)
        {
            InitializeComponent();
            lblError.Content = error;
        }
    }
}
