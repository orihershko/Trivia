#include "GameManager.h"
#include "IDataBase.h"


GameManager::GameManager(IDataBase *database)
	:m_database(database)
{
}

GameManager::GameManager(const GameManager & other)
{
	m_database = other.m_database;
	m_games = other.m_games;
}


GameManager::~GameManager()
{
}

Game GameManager::createGame(Room room)
{
	std::vector<Question> questions = m_database->getQuestions(room.getRoomData().questionCount); // insert the questions
	std::map<LoggedUser, GameData> players;

	for (std::string username : room.getAllUsers()) // insert all the player
	{
		GameData gameData;
		gameData.currentQuestion = nullptr;
		gameData.correctAnswerCount = 0;
		gameData.averangeAnswerTime = 0;
		gameData.wrongAnswerCount = 0;

		players[LoggedUser(username)] = gameData; // insert the player
	
	}
	Game game(questions, players);
	std::unique_lock<std::mutex> lck(m_mtx);
	m_games.push_back(game); // insert the new game
	lck.unlock();
	return game;
}

template <typename Map>
bool map_compare(Map const &lhs, Map const &rhs) {
	// No predicate needed because there is operator== for pairs already.
	return lhs.size() == rhs.size()
		&& std::equal(lhs.begin(), lhs.end(),
			rhs.begin());
}

void GameManager::deleteGame(Game game)
{
	for (std::vector<Game>::iterator it; it != m_games.end(); it++)
	{
		if (map_compare(it->getPlayers(), game.getPlayers()))
		{
			std::unique_lock<std::mutex> lck(m_mtx);
			m_games.erase(it);
			lck.unlock();
		}
			
	}
		
			

	throw std::exception("No such game");
}
