#include "GameRequestHandler.h"




GameRequestHandler::GameRequestHandler(LoggedUser user, Game& game, GameManager& gameManager, RequestHandlerFactory * handlerFactory)
	:m_user(user), m_game(game), m_gameManager(gameManager), m_handlerFactory(handlerFactory)
{
}

GameRequestHandler::~GameRequestHandler()
{
}

bool GameRequestHandler::isRequestRelevant(Request request)
{
	if (request.id == GET_QUESTION || request.id == SUBMIT_ANSWER || request.id == GET_GAME_RESULTS || request.id ==LEAVE_GAME)
		return true;

	//DO TO leave game
	return false;
}

RequestResult GameRequestHandler::handleRequest(Request request)
{
	if (request.id == GET_QUESTION)
		return getQuestion(request);

	else if (request.id == SUBMIT_ANSWER)
		return submitAnswer(request);

	else if (request.id == GET_GAME_RESULTS)
		return getGameResults(request);

	else if (request.id == LEAVE_GAME)
		return leaveGame(request);

	RequestResult requestResult;
	ErrorResponse res;
	res.message = UNKNOWN_REQUEST_ID_ERROR;
	requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	return requestResult;
	
}


std::vector<AnswerResponse> convert(std::vector<std::string> answersVector)
{
	std::vector<AnswerResponse> answers;
	for (unsigned int i = 0; i < answersVector.size(); i++)
	{
		AnswerResponse temp;
		temp.answer = answersVector[i];
		temp.option = i + 1;
		answers.push_back(temp);
	}
		
	

	return answers;
}

RequestResult GameRequestHandler::getQuestion(Request request)
{
	RequestResult requestResult;

	try
	{
		GetQuestionResponse res;
		Question quesion = m_game.getCurrentQuestion();

		res.question = quesion.getQuestion();
		std::vector<AnswerResponse> help = convert(quesion.getPossibleAnswers());
		
		for (int i = 0; i < help.size(); i++)
		{
			res.answers.push_back(help[i]);
		}

		res.status = SUCCESS;

		requestResult.response = JsonResponsePaketSerializer::serializeGetQuestionResponse(res);
	}
	catch (const std::exception& e)
	{
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}

	requestResult.newHandler = this;
	return requestResult;
}


RequestResult GameRequestHandler::submitAnswer(Request request)
{
	RequestResult requestResult;
	SubmitAnswerRequest userRequest = JsonRequestPacketDeserializer::deSerializeSubmitAnswerRequest(request.buffer);

	try
	{
		SubmitAnswerResponse res;
		res.correctAnswerId = m_game.getCurrentQuestion().getCorrectAnswerIndex();
		res.status = SUCCESS;

		requestResult.response = JsonResponsePaketSerializer::serializeSubmitAnswerResponse(res);
	}
	catch (const std::exception& e)
	{

		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}

	requestResult.newHandler = this;
	return requestResult;
}

RequestResult GameRequestHandler::getGameResults(Request request)
{
	RequestResult requestResult;
	try
	{
		GetGameResultsResponse res;
		std::vector<PlayerResults> results;
		for (auto player : m_game.getPlayers())
		{
			PlayerResults result;
			result.averageAnswerTime = player.second.averangeAnswerTime;
			result.correctAnswerCount = player.second.correctAnswerCount;
			result.wrongAnswerCount = player.second.wrongAnswerCount;
			result.username = player.first.getUsername();
			results.push_back(result);
		}
		res.results = results;
		res.status = SUCCESS;

		requestResult.response = JsonResponsePaketSerializer::serializeGetGameResultsResponse(res);
	}
	catch (const std::exception& e)
	{

		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}

	requestResult.newHandler = this;
	return requestResult;
}

RequestResult GameRequestHandler::leaveGame(Request request)
{
	RequestResult requestResult;

	try
	{
		LeaveRoomResponse res;

		res.status = SUCCESS;
		requestResult.response = JsonResponsePaketSerializer::serializeLeaveRoomResponse(res);
	}
	catch (const std::exception& e)
	{

		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}

	requestResult.newHandler = m_handlerFactory->createMenuRequestHandler(m_user);
	return requestResult;
}
