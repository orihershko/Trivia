#pragma once
#include <iostream>
#include <WinSock2.h>
#include <Windows.h>

class LoggedUser
{
public:
	LoggedUser(std::string username);
	~LoggedUser();
	
	std::string getUsername() const;
	bool operator==(const LoggedUser &loggedUser) const;
	bool operator<(const LoggedUser &loggedUser) const;
	bool operator>(const LoggedUser &loggedUser) const;

private:
	std::string _username;
};

