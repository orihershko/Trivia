#pragma once
#include <iostream>
#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include <map>
#include <list>
#include <vector>
#include "LoggedUser.h"
#include "Highscore.h"
#include "Question.h"

class IDataBase
{
public:
	virtual ~IDataBase() = default;
	virtual bool doesUserExists(std::string username) = 0;
	
	virtual void addUser(const std::string& userName, const std::string& password, const std::string& email) = 0;
	virtual void signIn(const std::string& userName, const std::string& password) = 0;

	virtual std::vector<Question> getQuestions(unsigned int numberOfQuestions) = 0;
	virtual std::vector<Highscore> getHighScores() = 0;
};

