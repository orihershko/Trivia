#pragma once
#include <iostream>
#include <vector>
#include "nlohmann\json.hpp"
#include "RoomManager.h"
#include "HighScoreTable.h"
#include"defines.h"
#include <map>


struct AnswerResponse
{
	unsigned int option;
	std::string answer;
};

struct GetQuestionResponse
{
	unsigned int status;
	std::string	question;
	std::vector<AnswerResponse> answers;
};

struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned int correctAnswerId;
};

struct PlayerResults
{
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
};

struct GetGameResultsResponse
{
	unsigned int status;
	std::vector<PlayerResults> results;
};


struct LoginResponse
{
	unsigned int status;
};

struct SignupResponse
{
	unsigned int status;
};

struct ErrorResponse
{
	std::string message;
};
struct SignoutResponse
{
	unsigned int status;
};
struct GetRoomResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
};
struct HighscoreResponse
{
	unsigned int status;
	std::vector<Highscore> highscore;
};
struct JoinRoomResponse
{
	unsigned int status;

};
struct CreateRoomResponse
{
	unsigned int status;
};
struct CloseRoomResponse
{
	unsigned int status;
};

struct StartGameResponse
{
	unsigned int status;
};

struct GetRoomStateResponse
{
	unsigned int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct LeaveRoomResponse
{
	unsigned int status;
};



struct JsonResponsePaketSerializer
{
	static Buffer serializeErrorResponse(ErrorResponse e)
	{
		json j = json{{ MESSAGE,e.message } };
		int id = ERROR_R;
		return makeMessage(j, id);

	}

	static Buffer serializeSignupResponse(SignupResponse e)
	{
		json j = json{ { STATUS,e.status } };
		int id = SIGNUP_R;
		return makeMessage(j, id);
	}

	static Buffer serializeLoginResponse(LoginResponse e)
	{
		json j = json{ { STATUS,e.status } };
		int id = LOGIN_R;
		return makeMessage(j, id);
	}

	static Buffer serializeSignoutResponse(SignoutResponse message)
	{
		json j = json{ {STATUS, message.status } };
		int id = SIGNOUT_R;
		return makeMessage(j, id);
	}

	static Buffer serializeGetRoomResponse(GetRoomResponse message)
	{
		json j;
		j[STATUS] = message.status;
		json jVec;
		for (unsigned int i = 0; i < message.rooms.size(); i++)
		{
			json temp;
			temp[ID] = message.rooms[i].id;
			temp[NAME] = message.rooms[i].name;
			temp[MAX_PLAYERS] = message.rooms[i].maxPlayers;
			temp[TIME_PER_QUESTION] = message.rooms[i].timePerQuestion;
			temp[IS_ACTIVE] = message.rooms[i].isActive;
			jVec.push_back(temp);
		}
		j[USERNAMES] = jVec.dump();
		int id = GET_ROOM_R;
		return makeMessage(j, id);
	}


	static Buffer serializeHighscoreResponse(HighscoreResponse message)
	{
		json j;
		j[STATUS] = message.status;
		json jVect;
		for (unsigned int i = 0; i < message.highscore.size(); i++)
		{
			json temp;
			temp[USERNAME] = message.highscore[i].username;
			temp[NUMBER_OF_GAME] = message.highscore[i].num_of_games;
			temp[WINS] = message.highscore[i].wins;
			temp[LOSES] = message.highscore[i].loses;
			temp[AVG_TIME] = message.highscore[i].avg_time;
			jVect.push_back(temp);
		}
		j[HIGHSCORE] = jVect;
		
		int id = HIGHSCORE_R;
		return makeMessage(j, id);
	}

	static Buffer serializeJoinRoomResponse(JoinRoomResponse message)
	{
		json j = json{ {STATUS,message.status} };
		int id = JOIN_ROOM_R;
		return makeMessage(j, id);
	}

	static Buffer serializeCreateRoomResponse(CreateRoomResponse message)
	{
		json j = json{ { STATUS,message.status } };
		int id = CREATE_ROOM_R;
		return makeMessage(j, id);
	}

	static Buffer serializeCloseRoomResponse(CloseRoomResponse message)
	{
		json j = json{ { STATUS,message.status } };
		int id = CLOSE_ROOM_R;
		return makeMessage(j, id);
	}

	static Buffer serializeStartGameResponse(StartGameResponse message)
	{
		json j = json{ { STATUS,message.status } };
		int id = START_GAME_R;
		return makeMessage(j, id);
	}

	static Buffer serializeGetRoomStateResponse(GetRoomStateResponse message)
	{
		json j = json{ { STATUS,message.status } };
		j[HAS_GAME_BEGUN] = message.hasGameBegun;
		j[PLAYERS] = message.players;
		j[QUESTION_COUNT] = message.questionCount;
		j[ANSWER_TIMEOUT] = message.answerTimeout;
		int id = GET_ROOM_STATE_R;
		return makeMessage(j, id);
	}

	static Buffer serializeLeaveRoomResponse(LeaveRoomResponse message)
	{
		json j = json{ { STATUS,message.status } };
		int id = LEAVE_ROOM_R;
		return makeMessage(j, id);
	}

	static Buffer serializeGetQuestionResponse(GetQuestionResponse message)
	{
		json j = json{ { STATUS,message.status } };
		j[QUESTION] = message.question;
		json jvec;
		for (int i = 0; i < message.answers.size(); i++)
		{
			json temp;
			temp[OPTION] = message.answers[i].option;
			temp[ANSWER] = message.answers[i].answer;
			jvec.push_back(temp);
		}
		
		j[ANSWERS] = jvec;
		int id = GET_QUESTION_R;
		return makeMessage(j, id);
	}

	static Buffer serializeSubmitAnswerResponse(SubmitAnswerResponse message)
	{
		json j = json{ { STATUS,message.status } };
		j[CORRECT_ANSWER_ID] = message.correctAnswerId;
		int id = SUBMIT_ANSWER_R;
		return makeMessage(j, id);
	}

	static Buffer serializeGetGameResultsResponse(GetGameResultsResponse message)
	{
		json j = json{ { STATUS,message.status } };
		json jVector;
		for (unsigned int i = 0; i < message.results.size(); i++)
		{
			json temp;
			temp[USERNAME] = message.results[i].username;
			temp[CORRECT_ANSWER_COUNT] = message.results[i].correctAnswerCount;
			temp[WRONG_ANSWER_COUNT] = message.results[i].wrongAnswerCount;
			temp[AVERAGE_ANSWER_TIME] = message.results[i].averageAnswerTime;
			jVector.push_back(temp);
		}
		j[RESULTS] = jVector;
		int id = GET_GAME_RESULTS_R;
		return makeMessage(j, id);
	}

	

	static Buffer makeMessage(json j, int id)
	{
		std::cout << "make json:\n" + j.dump() << std::endl;
		std::vector<uint8_t> contents = json::to_cbor(j);
		int lenMess = (int)contents.size();
		uint8_t id2 = (uint8_t)id;
		uint32_t lenMess2 = (uint32_t)lenMess;
		uint8_t lenInByte[4];
		lenInByte[0] = (uint8_t)lenMess2; //lsb
		lenInByte[1] = (uint8_t)(lenMess2 >>= 8); //shift
		lenInByte[2] = (uint8_t)(lenMess2 >>= 8);
		lenInByte[3] = (uint8_t)(lenMess2 >>= 8);
		std::vector<uint8_t> message;
		message.push_back(id2); //push id
		for (int i = 0; i < 4; i++) //push the size of the message
			message.push_back(lenInByte[i]);
		message.insert(message.end(), contents.begin(), contents.end()); // push the contents of the message
		return message;
	}
};
