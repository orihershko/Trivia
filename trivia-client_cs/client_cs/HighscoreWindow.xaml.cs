﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace client_cs
{
    /// <summary>
    /// Interaction logic for HighscoreWindow.xaml
    /// </summary>
    public partial class HighscoreWindow : Window
    {
        Client _client;
        public HighscoreWindow(Client c)
        {
            _client = c;
            InitializeComponent();
            List<Highscore> h = _client.HallOfFame();
            h.Sort();
            Label[] label = new Label[3];
            label[0] = LblP1;
            label[1] = LblP2;
            label[2] = LblP3;
            for (int i=0;i<h.Count;i++)
            {
                if(i==3)
                {
                    break;
                }
                label[i].Content = h[h.Count-i-1].username;
            }
           


        }

        private void ButBack_Click(object sender, RoutedEventArgs e)
        {
            Menu menu = new Menu(_client);
            MediaPlayer Sound1 = new MediaPlayer();
            Sound1.Open(new Uri(@"C:\Users\magshimim\source\repos\client_cs\client_cs\bin\Debug\sounds\tick.wav"));
            Sound1.Play();

            this.Close();
            menu.Show();
            Close();
        }
    }
}
