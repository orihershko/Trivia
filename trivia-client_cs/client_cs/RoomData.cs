﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client_cs
{
    public class RoomData
    {
        public uint id;
        public string name;
        public uint maxPlayers;
        public int timePerQuestion;
        public uint numOfQuestion;
    }
}
