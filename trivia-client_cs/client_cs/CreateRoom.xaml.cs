﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace client_cs
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    public partial class CreateRoom : Window
    {
        Client _client;
        public CreateRoom(Client c)
        {
            _client = c;
            InitializeComponent();
        }

        private void ButRest_Click(object sender, RoutedEventArgs e)
        {
            TxtNumOfPlayers.Text = "";
            TxtNumOfQuestion.Text = "";
            TxtRoomName.Text = "";
            TxtTimeForQuestion.Text = "";
        }

        private void ButBack_Click(object sender, RoutedEventArgs e)
        {
            Menu menu = new Menu(_client);
            MediaPlayer Sound1 = new MediaPlayer();
            Sound1.Open(new Uri(@"C:\Users\magshimim\source\repos\client_cs\client_cs\bin\Debug\sounds\tick.wav"));
            Sound1.Play();
           
            this.Close();
            menu.Show();
            Close();
        }

        private void ButSend_Click(object sender, RoutedEventArgs e)
        {
           
            if (TxtNumOfPlayers.Text == "" || TxtNumOfQuestion.Text == "" || TxtRoomName.Text == ""|| TxtTimeForQuestion.Text == "")
            {
                throw new ArgumentException("not all filed, try again");

            }
            RoomData data = new RoomData
            {
                maxPlayers = uint.Parse(TxtNumOfPlayers.Text),
                numOfQuestion = uint.Parse(TxtNumOfQuestion.Text),
                timePerQuestion = int.Parse(TxtTimeForQuestion.Text),
                name = TxtRoomName.Text
            };
            _client.CreateRoom(TxtNumOfPlayers.Text, TxtNumOfQuestion.Text, TxtRoomName.Text, TxtTimeForQuestion.Text);


            Room room = new Room(_client, data, true);
            room.Show();
            Close();
        }
    }
}
