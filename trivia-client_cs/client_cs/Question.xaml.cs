﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
namespace client_cs
{
    /// <summary>
    /// Interaction logic for Question.xaml
    /// </summary>
    public partial class Question : Window
    {
        Client _client;
        int _time = 1000;
        DispatcherTimer _timer = new System.Windows.Threading.DispatcherTimer();
        uint _numOfQ;
        Responses.GetQuestionResponse questionResponse;
        int _QuestionHave;
        int _staticTime;

        public Question(Client c,int time,uint numOfQ, int QuestionHave)
        {
            
            _client = c;
            _numOfQ = numOfQ;
            _QuestionHave = QuestionHave;
            _time = time;
            _staticTime = _time;
            _timer = new DispatcherTimer
            {
                Interval = new TimeSpan(0, 0, 1)
            };
            _timer.Tick += Timer_Tick;
            _timer.Start();
            if (_numOfQ< _QuestionHave)
            {
                _timer.Stop();
                Responses.GetGameResultsResponse playerResults = _client.GetGameResults();
                string username = _client.GetUsername();
                for (int i =0;i< playerResults.results.Count;i++)
                {
                    if(username == playerResults.results[i].username)
                    {
                        // Do To print all the stats
                        Menu menu = new Menu(c);
                        menu.Show();
                        Close();
                    }
                }
                
            }
          
            InitializeComponent();

            questionResponse = _client.GetQuestion();
            lblQuestion.Content = questionResponse.question;
            but1.Content = questionResponse.answers[Defines.OPTION1].answer;
            but2.Content = questionResponse.answers[Defines.OPTION2].answer;
            but3.Content = questionResponse.answers[Defines.OPTION3].answer;
            but4.Content = questionResponse.answers[Defines.OPTION4].answer;

            lblQ.Content = numOfQ.ToString() + "/" + _QuestionHave.ToString(); 
           
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
           
            Time.Content = _time.ToString();
            _time--;
            if(_time==0)
            {
                
                Question question = new Question(_client, _staticTime, _numOfQ , _QuestionHave++);
                question.ShowDialog();
                Close();
            }

        }

        private void But4_Click(object sender, RoutedEventArgs e)
        {
            
            Responses.SubmitAnswerResponse response = _client.SubmitAnswer((int)questionResponse.answers[Defines.OPTION4].option);
            if(response.correctAnswerId== Defines.OPTION4)
            {
                but4.Background = Brushes.Green;
            }
            else
            {
                but4.Background = Brushes.Red;
            }
        }

        private void But3_Click(object sender, RoutedEventArgs e)
        {

            Responses.SubmitAnswerResponse response = _client.SubmitAnswer((int)questionResponse.answers[Defines.OPTION3].option);
            if (response.correctAnswerId == Defines.OPTION3)
            {
                but3.Background = Brushes.Green;
            }
            else
            {
                but3.Background = Brushes.Red;
            }
        }

        private void But2_Click(object sender, RoutedEventArgs e)
        {

            Responses.SubmitAnswerResponse response = _client.SubmitAnswer((int)questionResponse.answers[Defines.OPTION2].option);
            if (response.correctAnswerId == Defines.OPTION2)
            {
                but2.Background = Brushes.Green;

            }
            else
            {
                but2.Background = Brushes.Red;
            }
        }

        private void But1_Click(object sender, RoutedEventArgs e)
        {

            Responses.SubmitAnswerResponse response = _client.SubmitAnswer((int)questionResponse.answers[Defines.OPTION1].option);
            if (response.correctAnswerId == Defines.OPTION1)
            {
                but1.Background = Brushes.Green;
            }
            else
            {
                but1.Background = Brushes.Red;
            }
        }

        private void ButExit_Click(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            _client.LeaveGame();
            Menu menu = new Menu(_client);
            menu.Show();
            Close();
        }
    }
}
