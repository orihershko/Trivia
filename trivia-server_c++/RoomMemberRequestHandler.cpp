#include "RoomMemberRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(LoggedUser & user, RoomManager & roomManager, Room & room, RequestHandlerFactory * handlerFacroty)
	:m_handlerFacroty(handlerFacroty), m_roomManager(roomManager), m_user(user), m_room(room)
{
}

RoomMemberRequestHandler::~RoomMemberRequestHandler()
{
}

bool RoomMemberRequestHandler::isRequestRelevant(Request request)
{
	if (request.id == GET_ROOM_STATE || request.id == LEAVE_ROOM)
		return true;

	return false;
}

RequestResult RoomMemberRequestHandler::handleRequest(Request request)
{
	
	if (request.id == GET_ROOM_STATE)
		return getRoomState(request);

	else if (request.id == LEAVE_ROOM)
		return leaveRoom(request);
	RequestResult requestResult;
	ErrorResponse res;

	res.message = UNKNOWN_REQUEST_ID_ERROR;
	requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);

	return requestResult;
}

RequestResult RoomMemberRequestHandler::leaveRoom(Request)
{
	RequestResult requestResult;

	try
	{
		LeaveRoomResponse res;

		m_room.removeUser(m_user);
		res.status = SUCCESS;

		requestResult.response = JsonResponsePaketSerializer::serializeLeaveRoomResponse(res);
		requestResult.newHandler = m_handlerFacroty->createMenuRequestHandler(m_user);
	}
	catch (const std::exception& e)
	{
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
		requestResult.newHandler = this;
	}

	return requestResult;
}

RequestResult RoomMemberRequestHandler::getRoomState(Request)
{
	RequestResult requestResult;

	try
	{
		GetRoomStateResponse res;

		res.status = SUCCESS;
		res.hasGameBegun = m_room.getRoomData().isActive;
		res.players = m_room.getAllUsers();
		res.answerTimeout = m_room.getRoomData().timePerQuestion;
		res.questionCount = m_room.getRoomData().questionCount;

		requestResult.response = JsonResponsePaketSerializer::serializeGetRoomStateResponse(res);
	}
	catch (const std::exception& e)
	{
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}

	requestResult.newHandler = this;
	return requestResult;
}

LoggedUser RoomMemberRequestHandler::getUser()
{
	return m_user;
}
