#pragma once
#include <iostream>
#include "LoginManager.h"
#include "RequestHandlerFactory.h"
#include "RequestResult.h"
#include "Request.h"
#include "Responses.h"
#include "defines.h"


class RequestHandlerFactory;
class MenuRequestHandler;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(LoginManager &loginManager, RequestHandlerFactory *handlerFactory);
	~LoginRequestHandler();

	bool isRequestRelevant(Request request) override;
	RequestResult handleRequest(Request request) override;

	RequestResult login(Request request);
	RequestResult signup(Request request);

private:
	LoginManager _loginManager;
	RequestHandlerFactory *_handlerFactory;
};

