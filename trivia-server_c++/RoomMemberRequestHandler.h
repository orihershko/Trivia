#pragma once
#include <iostream>
#include "LoginManager.h"
#include "RequestHandlerFactory.h"
#include "RequestResult.h"
#include "Request.h"
#include "Responses.h"
#include "defines.h"
class RequestHandlerFactory;


class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(LoggedUser& user, RoomManager& roomManager, Room& room, RequestHandlerFactory *handlerFacroty);
	~RoomMemberRequestHandler();

	bool isRequestRelevant(Request request) override;
	RequestResult handleRequest(Request request) override;

	RequestResult leaveRoom(Request);
	RequestResult getRoomState(Request);

	LoggedUser getUser();

private:
	RequestHandlerFactory * m_handlerFacroty;
	RoomManager m_roomManager;
	LoggedUser m_user;
	Room m_room;
};

