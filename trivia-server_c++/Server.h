#pragma once
#include "IDataBase.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"

class Server
{
public:
	Server();
	~Server();

	void run();

private:
	IDataBase *_database;
	Communicator _communicator;
	RequestHandlerFactory _handlerFactory;
};