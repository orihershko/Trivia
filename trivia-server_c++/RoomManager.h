#pragma once
#include <iostream>
#include <vector>
#include <map>
#include<mutex>

#include "LoggedUser.h"
#include "RoomData.h"
#include "Room.h"

class RoomManager
{
public:
	RoomManager() = default;
	~RoomManager();
	RoomManager(const RoomManager& other);

	Room* createRoom(RoomData roomData);
	void deleteRoom(int ID);
	unsigned int getRoomState(int ID);
	std::vector<RoomData> getRooms();
	void signout_user(LoggedUser user);
	Room* signin_user(LoggedUser user, unsigned int roomId);
	std::vector<std::string> getPlayersInRoom(int ID);
	

private:
	std::mutex m_mtx;
	std::map<unsigned int, Room*> m_rooms;
	Room& getRoom(unsigned int roomId);
};