#pragma once

struct Highscore
{
	std::string username;
	unsigned int num_of_games;
	unsigned int wins;
	unsigned int loses;
	unsigned int avg_time;
};