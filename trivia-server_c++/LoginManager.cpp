#include "LoginManager.h"

LoginManager::LoginManager(IDataBase *database) : _database(database)
{

}

LoginManager::~LoginManager()
{
}


void LoginManager::signup(const std::string username, const std::string password, const std::string email)
{
	if (_database->doesUserExists(username))
		throw std::exception("User exists");
	_database->addUser(username, password, email);
}


void LoginManager::login(const std::string username, const std::string password)
{
	if (!_database->doesUserExists(username))
		throw std::exception("User not exists");

	_database->signIn(username, password);
}


void LoginManager::logout(const std::string username, const std::string password)
{
	//DOTO
}


IDataBase *LoginManager::getDatabase()
{
	return _database;
}

