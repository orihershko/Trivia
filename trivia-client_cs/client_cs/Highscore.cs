﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client_cs
{
    public class Highscore : IComparable<Highscore>
    {
        public string username;
        public uint num_of_game;
        public uint wins;
        public uint loses;
        public uint avg_time;

        public int CompareTo(Highscore other)
        {
            if (wins > other.wins)
                return 1;
            else if (wins < other.wins)
                return -1;
            return 0;
        }
    }
}
