﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;
using Newtonsoft.Json;
using System.Windows.Media;
static class Constants
{
    public const string DIRECT_CONFIG = "config.txt";
}
namespace client_cs
{
    public class Client
    {
        int _port;
        string _ip;
        TcpClient _client;
        NetworkStream _stream;
        string _username;
        Helper helper = new Helper(); 
       public Client()
       {
            Configuration();
            try
            {
                _client = new TcpClient(_ip, _port); // connect
                _stream = _client.GetStream();
            }
            catch(SocketException)
            {
                throw new ArgumentException("serevr don't work");
            }
            
            


       }
        
        private void Configuration()
        {
            if (!System.IO.File.Exists(Constants.DIRECT_CONFIG))
            {
                throw new ArgumentException("config file does not exist");
            }
            string[] data = System.IO.File.ReadAllLines(Constants.DIRECT_CONFIG);
            _port = int.Parse(data[1].Substring(data[1].IndexOf("=") + 1));
            _ip = data[0].Substring(data[0].IndexOf("=") + 1);
        }
        public string GetUsername()
        {
            return _username;
        }
        public void GetLogin(string name,string pass)
        {
            try
            {
                _username = name;
                Requests.LoginRequest login = new Requests.LoginRequest
                {
                    password = pass,
                    username = name


                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeLoginRequest(login);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
            }
            catch(Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
            
           
        }
        public void Signup(string name, string pass, string mail)
        {
            try
            {
                _username = name;
                Requests.SignupRequest signup = new Requests.SignupRequest
                {
                    username = name,
                    password = pass,
                    email = mail

                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeSignupRequest(signup);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
            }
            catch (Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
          
           

        }
        public void Exit()
        {
            _stream.Close();
        }
        public void CreateRoom(string numOfPlayers,string NumOfQuestion,string RoomName,string TimeForQuestion)
        {
            try
            {
                Requests.CreateRoomRequest createRoom = new Requests.CreateRoomRequest
                {
                    answerTimeout = uint.Parse(TimeForQuestion),
                    maxUsers = uint.Parse(numOfPlayers),
                    roomName = RoomName,
                    questionCount = uint.Parse(NumOfQuestion)


                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeCreateRoomRequest(createRoom);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
            }
            catch (Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
           

        }
        public Highscore MyStatus()
        {
            try
            {
                Requests.HighscoreRequest highscore = new Requests.HighscoreRequest
                {
                    status = Defines.SUCCESS


                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeHighscoreRequest(highscore);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
                Responses.HighscoreResponse response = Responses.JsonResponsePaketDeserializer.DeSerializeHighscoreResponse(request._buffer);
                for (int i = 0; i < response.highscore.Count; i++)
                {
                    if (response.highscore[i].username == _username)
                    {
                        return response.highscore[i];
                    }
                }
            }
            catch (Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
            Highscore t = new Highscore
            {
                avg_time = 0,
                loses = 0,
                num_of_game = 0,
                username = _username,
                wins = 0,
            };
            return t;
        }
        public List<Highscore> HallOfFame()
        {
            try
            {
                Requests.HighscoreRequest highscore = new Requests.HighscoreRequest
                {
                    status = Defines.SUCCESS


                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeHighscoreRequest(highscore);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
                Responses.HighscoreResponse response = Responses.JsonResponsePaketDeserializer.DeSerializeHighscoreResponse(request._buffer);
                return response.highscore;
            }
            catch (Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
            Responses.HighscoreResponse t = new Responses.HighscoreResponse();
            for(int i =0;i<Defines.MAX_HALL_OF_FAME;i++)
            {
                Highscore highscore = new Highscore
                {
                    avg_time = 0,
                    loses = 0,
                    num_of_game = 0,
                    username = "",
                    wins = 0,
                };
                t.highscore.Add(highscore);

            }
            return t.highscore;
        }

        public List<RoomData> GetRooms()
        {
            try
            {
                Requests.GetRoomsRequest Rooms = new Requests.GetRoomsRequest
                {
                    status = Defines.SUCCESS
                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeGetRoomsRequest(Rooms);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
                Responses.GetRoomResponse response = Responses.JsonResponsePaketDeserializer.DeSerializeGetRoomResponse(request._buffer);
                return response.rooms;
            }
            catch (Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
            Responses.GetRoomResponse t = new Responses.GetRoomResponse();
            return t.rooms;


        }
        public void GetJoinRoom(uint id)
        {
            try
            {
                Requests.JoinRoomRequest Room = new Requests.JoinRoomRequest
                {
                    roomId = id
                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeJoinRoomRequest(Room);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
            }
            catch (Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
            
           
        }
        public Responses.GetRoomStateResponse GetRoomState()
        {
            try
            {
                Requests.GetRoomStateRequest state = new Requests.GetRoomStateRequest
                {
                    status = Defines.SUCCESS
                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeGetRoomStateRequest(state);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
                Responses.GetRoomStateResponse response = Responses.JsonResponsePaketDeserializer.DeSerializeGetRoomStateResponse(request._buffer);
                return response;
            }
            catch (Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
           
            Responses.GetRoomStateResponse t = new Responses.GetRoomStateResponse();
            return t;
        }

        public Responses.GetQuestionResponse GetQuestion()
        {
            try
            {
                Requests.GetQuestionRequest req = new Requests.GetQuestionRequest
                {
                    status = Defines.SUCCESS
                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeGetQuestionRequest(req);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
                Responses.GetQuestionResponse response = Responses.JsonResponsePaketDeserializer.DeSerializeGetQuestionResponse(request._buffer);
                return response;
            }
            catch (Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
            Responses.GetQuestionResponse t = new Responses.GetQuestionResponse();
            return t;
        }

        public Responses.SubmitAnswerResponse SubmitAnswer(int op)
        {
            try
            {
                Requests.SubmitAnswerRequest req = new Requests.SubmitAnswerRequest
                {
                    answerId = (uint)op
                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeSubmitAnswerRequest(req);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
                Responses.SubmitAnswerResponse response = Responses.JsonResponsePaketDeserializer.DeSerializeSubmitAnswerResponse(request._buffer);
                return response;
            }
            catch (Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
            Responses.SubmitAnswerResponse t = new Responses.SubmitAnswerResponse();
            return t;
        }

        public Responses.GetGameResultsResponse GetGameResults()
        {
            try
            {
                Requests.GetGameResultRequest req = new Requests.GetGameResultRequest
                {
                    status = Defines.SUCCESS
                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeGetGameResultRequest(req);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
                Responses.GetGameResultsResponse response = Responses.JsonResponsePaketDeserializer.DeSerializeGetGameResultsResponse(request._buffer);
                return response;
            }
            catch (Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
            Responses.GetGameResultsResponse t = new Responses.GetGameResultsResponse();
            return t;
        }

        public void LeaveGame()
        {
            try
            {
                Requests.LeaveGameRequest req = new Requests.LeaveGameRequest
                {
                    status = Defines.SUCCESS
                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeLeaveGameRequest(req);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
            }
            catch (Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
           
          
            
        }

        public void StartGame()
        {
            try
            {
                Requests.StartGameRequest req = new Requests.StartGameRequest
                {
                    status = Defines.SUCCESS
                };
                byte[] buffer = Requests.JsonRequestPacketSerializer.SerializeStartGameRequest(req);
                _stream.Write(buffer, 0, buffer.Length);
                Request request = helper.GetRequest(helper.GetId(_stream), helper.GetMessage(_stream, helper.GetLength(_stream)));
                if (request._id == Defines.ERROR_R)
                {
                    Responses.ErrorResponse error = Responses.JsonResponsePaketDeserializer.DeSerializeErrorResponse(request._buffer);
                    throw new ArgumentException(error.message);
                }
            }
            catch (Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play();
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
            }
           
        }

       
    }
}
