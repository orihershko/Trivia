﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace client_cs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       
        private SoundPlayer Player = new SoundPlayer();

        public MainWindow()
        {
            InitializeComponent();
           this.Player.SoundLocation = "sounds/soundtrack.wav";
           this.Player.PlayLooping();
            try
            {
                Login m = new Login();
                m.ShowDialog();
                Close();
                /*Question q = new Question();
                q.Show();
                Close();*/
                

            }
            catch(Exception error)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                this.Player.Stop();
                Sound1.Open(new Uri("pack://siteoforigin:,,,sounds/error.mp3"));
                Sound1.Play(); 
                errorWin errorWin = new errorWin(error.ToString());
                errorWin.ShowDialog();
               
                this.Close();
            }
           
        }

       
       
    }
}
