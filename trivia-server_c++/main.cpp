#pragma comment (lib, "ws2_32.lib")

#include <iostream>
#include "Server.h"
#include "WSAInitializer.h"
#include <thread>

int main()
{
	// Level 4
	WSAInitializer wsaInit;

	Server server;
	server.run();

	system("PAUSE");
	return 0;
}