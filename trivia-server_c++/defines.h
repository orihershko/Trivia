#pragma once
#include"nlohmann\json.hpp"
using Buffer = std::vector<uint8_t>;
using json = nlohmann::json;
#define UNKNOWN_REQUEST_ID_ERROR "Unknown request id for current handler"
#define MESSAGE "message"
#define STATUS "status"
#define USERNAMES "usernames"
#define HIGHSCORE "highscore"
#define NUMBER_OF_GAME "num_of_game"
#define WINS "wins"
#define LOSES "loses"
#define AVG_TIME "avg_time"
#define ID "id"
#define NAME "name"
#define MAX_PLAYERS "maxPlayers"
#define TIME_PER_QUESTION "timePerQuestion"
#define IS_ACTIVE "isActive"
#define USERNAME "username"
#define PASSWORD "password"
#define EMAIL "email"
#define ROOM_ID "roomId"
#define ROOM_NAME "roomName"
#define MAX_USERS "maxUsers"
#define QUESTION_COUNT "questionCount"
#define ANSWER_TIMEOUT "answerTimeout"
#define HAS_GAME_BEGUN "hasGameBegun" 
#define PLAYERS "players"
#define QUESTION_COUNT "questionCount"
#define ANSWER_TIMEOUT "answerTimeout"
#define QUESTION "question"
#define ANSWERS "answers"
#define CORRECT_ANSWER_ID "correctAnswerId"
#define RESULTS "results"
#define CORRECT_ANSWER_COUNT "correctAnswerCount"
#define WRONG_ANSWER_COUNT "wrongAnswerCount"
#define AVERAGE_ANSWER_TIME "averageAnswerTime"
#define ANSWER_ID "answerId"
#define ANSWER "answer"
#define OPTION "option"


//response defines:
#define LOGIN_R 100
#define SIGNUP_R 101
#define ERROR_R 102
#define LOGOUT_R 103
#define GET_ROOM_R 104
#define GET_PLAYERS_IN_ROOM_R 105
#define HIGHSCORE_R 106
#define JOIN_ROOM_R 107
#define CREATE_ROOM_R 108
#define SIGNOUT_R 109
//Admin:
#define CLOSE_ROOM_R 110
#define START_GAME_R 111
#define GET_ROOM_STATE_R 112
//Member:
#define LEAVE_ROOM_R 113
//game resonse defines:
#define GET_QUESTION_R 114
#define SUBMIT_ANSWER_R 115
#define GET_GAME_RESULTS_R 116
#define PLAYERS_RESULTS_R 117
#define LEAVE_GAME_R 118
#define GET_GAME_RESULT_R 119

//login request defines:
const int SUCCESS = 1;
#define LOGIN 100
#define SIGNUP 101

//menu request defines:
#define GET_PLAYERS_IN_ROOM 102
#define JOIN_ROOM 103
#define CREATE_ROOM 104
#define HIGHSCORE_ID 105
#define GET_ROOMS 106
#define SIGNOUT 107

// Admin room request defines:
#define CLOSE_ROOM 108
#define START_GAME 109
#define GET_ROOM_STATE 110

//room request defines:
#define LEAVE_ROOM 111

//game request defines:
#define GET_QUESTION 112
#define SUBMIT_ANSWER 113
#define GET_GAME_RESULTS 114
#define PLAYERS_RESULTS 115
#define LEAVE_GAME 116
#define GET_GAME_RESULT 117


