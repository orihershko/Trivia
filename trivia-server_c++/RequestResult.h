#pragma once
#include "IRequestHandler.h"
#include "Room.h"

class IRequestHandler;
using Buffer = std::vector<uint8_t>;

struct RequestResult
{
	Buffer response;
	IRequestHandler* newHandler;
	bool gameStarted;
	Room *room;
};
