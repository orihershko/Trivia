#pragma once
#include <iostream>
#include <string>
#include <vector>

#include "IDataBase.h"
#include "SQLDataBase.h"
#include "LoggedUser.h"

using std::cout;
using std::endl;

class LoginManager
{
public:
	LoginManager(IDataBase *database);
	~LoginManager();

	void signup(const std::string username, const std::string password, const std::string);
	void login(const std::string username, const std::string password);
	void logout(const std::string username, const std::string password);

	IDataBase *getDatabase();
private:
	IDataBase *_database;
};

