﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace client_cs
{
    /// <summary>
    /// Interaction logic for Room.xaml
    /// </summary>
    public partial class Room : Window
    {
        Client _client;
        RoomData _roomData;
        bool _isAdmin;
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        public Room(Client c,RoomData data, bool isAdmin)
        {
            _client = c;
            _roomData = data;
            _isAdmin = isAdmin;
            if(!_isAdmin)
            {
                butStart.Visibility = Visibility.Hidden;
            }
            
            dispatcherTimer.Tick += DispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
            

           
            InitializeComponent();
            timePerQ.Content += " " + data.timePerQuestion.ToString();
            maxPlayers.Content += " " + data.maxPlayers.ToString();
            numOfQ.Content += " " + data.numOfQuestion.ToString();
        }

        private void ButClose_Click(object sender, RoutedEventArgs e)
        {
            Menu menu = new Menu(_client);
            menu.Show();
            Close();
        }
        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            listBox.Items.Clear();
            Responses.GetRoomStateResponse res = _client.GetRoomState();
            if (res.hasGameBegun)
            {
                Question question = new Question(_client,(int) _roomData.timePerQuestion, _roomData.numOfQuestion, Defines.START);
                question.Show();
                Close();
            }
            for (int i = 0; i < res.players.Count; i++)
            {
                listBox.Items.Add(res.players[i]);
            }
            

        }
        private void ButStart_Click(object sender, RoutedEventArgs e)
        {
            _client.StartGame();
            dispatcherTimer.Stop();
             Question question = new Question(_client,(int) _roomData.timePerQuestion, _roomData.numOfQuestion, Defines.START);
            question.ShowDialog();
            Close();
        }
        

      
    }
}
