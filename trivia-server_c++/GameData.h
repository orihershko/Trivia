#pragma once
#include "Question.h"

struct GameData
{
	Question *currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averangeAnswerTime;

	bool operator==(const GameData& gameData) const
	{
		return correctAnswerCount == gameData.correctAnswerCount
			&& wrongAnswerCount == gameData.wrongAnswerCount
			&& averangeAnswerTime == gameData.averangeAnswerTime
			&& currentQuestion == gameData.currentQuestion;
	}
};