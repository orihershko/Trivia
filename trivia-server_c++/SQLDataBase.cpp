#include "SQLDataBase.h"
#define DONT_EXIST -1
#include<io.h>
void check(int exec_res, char *errMessage)
{
	errMessage = nullptr;
	if (exec_res != SQLITE_OK)
	{
		cout << "Error message: " << endl;
		system("PAUSE");
	}
		
}
bool SQLDataBase::open()
{
	int res = sqlite3_open(m_FileName.c_str(), &m_db);
	return res == SQLITE_OK;
}

//if the sql have answer. it will enter to callback
int callbackDoesUserMatching(void *data, int argc, char **argv, char **azColName)
{
	bool *result = static_cast<bool*>(data);
	*result = true;

	return 0;
}


SQLDataBase::SQLDataBase()
{
	m_FileName = "MyTriviaDB.sqlite";
	int doesFileExist = _access(m_FileName.c_str(), 0); //0 =exists / -1 = doesnt exist
	if (!open())
	{
		m_db = nullptr;
		std::cout << "Faild to open DB\n" << std::endl;
	}

	if (doesFileExist == DONT_EXIST)
	{
		std::string sqlStatement = "Create table User(username TEXT PRIMARY KEY, password TEXT NOT NULL,email TEXT NOT NULL);";
		char *errMessage = nullptr;
		int res = sqlite3_exec(m_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		check(res, errMessage);


		sqlStatement = "Create table Question(question_id INTEGER PRIMARY KEY, question TEXT NOT NULL,correct_ans TEXT NOT NULL,ans2 TEXT NOT NULL,ans3 TEXT NOT NULL,ans4 TEXT NOT NULL);";
		errMessage = nullptr;
		res = sqlite3_exec(m_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		check(res, errMessage);

		sqlStatement = "Create table Game(game_id INTEGER PRIMARY KEY, status TEXT NOT NULL,start_time INTEGER NOT NULL,end_time INTEGER NOT NULL);";
		errMessage = nullptr;
		res = sqlite3_exec(m_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		check(res,errMessage);

		sqlStatement = "Create table statistic(username TEXT PRIMARY KEY, num_of_game INTEGER NOT NULL, wins INTEGER NOT NULL, loses INTEGER NOT NULL, avg_time INTEGER NOT NULL);";
		errMessage = nullptr;
		res = sqlite3_exec(m_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		check(res, errMessage);
	}


}


SQLDataBase::~SQLDataBase()
{
	sqlite3_close(m_db);
}


bool SQLDataBase::doesUserExists(std::string username)
{
	std::string sqlStatement ="select * from User where username = '" + username + "';";
	bool result = false;
	char *errMessage = nullptr;

	int res = sqlite3_exec(m_db, sqlStatement.c_str(), callbackDoesUserMatching, (void*)&result, &errMessage);
	check(res, errMessage);

	return result; 
}


void SQLDataBase::addUser(const std::string& userName, const std::string& password, const std::string& email)
{
	std::string sqlStatement = "INSERT INTO User VALUES('" + userName + "','" + password + "','" + email + "');";
	char *errMessage = nullptr;

	int res = sqlite3_exec(m_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	check(res, errMessage);
}


void SQLDataBase::signIn(const std::string& userName, const std::string& password)
{
	std::string sqlStatement = std::string("select username from User where username = '" + userName + "' AND password = '" + password + "';");
	bool result = false;
	char *errMessage = nullptr;

	int res = sqlite3_exec(m_db, sqlStatement.c_str(), callbackDoesUserMatching, (void*)&result, &errMessage);
	check(res, errMessage);

	if (!result)
		throw std::exception("FAILED TO LOGIN");
}

// reate table statistic(username TEXT PRIMARY KEY, num_of_game INTEGER NOT NULL, wins INTEGER NOT NULL, loses INTEGER NOT NULL, avg_time INTEGER NOT NULL);";
int loadHighscore(void *data, int argc, char **argv, char **azColName)
{
	std::vector<Highscore> *highScores = static_cast<std::vector<Highscore>*>(data);
	Highscore h;

	for (int i = 0; i < argc; i++) {

		if (std::string(azColName[i]) == "username") {
			h.username = argv[i];
		}
		else if (std::string(azColName[i]) == "num_of_game") {
			h.num_of_games = atoi(argv[i]);
		}
		else if (std::string(azColName[i]) == "wins") {
			h.wins = atoi(argv[i]);
		}
		else if (std::string(azColName[i]) == "loses") {
			h.loses = atoi(argv[i]);
		}
		else if (std::string(azColName[i]) == "avg_time") {
			h.avg_time = atoi(argv[i]);
		}
	}
	highScores->push_back(h);
	return 0;
}

int loadQuestion(void *data, int argc, char **argv, char **azColName)
{
	std::vector<Question> *questions = static_cast<std::vector<Question>*>(data);
	std::string question, correct_ans, ans2, ans3, ans4;

	for (int i = 0; i < argc; i++) {

		if (std::string(azColName[i]) == "question") {
			question = argv[i];
		}
		else if (std::string(azColName[i]) == "correct_ans") {
			correct_ans = argv[i];
		}
		else if (std::string(azColName[i]) == "ans2") {
			ans2 = argv[i];
		}
		else if (std::string(azColName[i]) == "ans3") {
			ans3 = argv[i];
		}
		else if (std::string(azColName[i]) == "ans4") {
			ans4 = argv[i];
		}
	}
	Question q(question, std::vector<std::string>({ correct_ans, ans2, ans3, ans4 }), 0);
	questions->push_back(q);

	return 0;
}

std::vector<Highscore> SQLDataBase::getHighScores()
{
	std::string sqlStatement = "SELECT * FROM statistic";
	std::vector<Highscore> *highScores = new std::vector<Highscore>();
	char *errMessage = nullptr;

	int res = sqlite3_exec(m_db, sqlStatement.c_str(), loadHighscore, (void*)(highScores), &errMessage);
	check(res, errMessage);
	
	return *highScores;
}

std::vector<Question> SQLDataBase::getQuestions(unsigned int numberOfQuestions)
{
	std::string sqlStatement = "SELECT * FROM Question;";
	std::vector<Question> *questions = new std::vector<Question>();
	char *errMessage = nullptr;

	int res = sqlite3_exec(m_db, sqlStatement.c_str(), loadQuestion, (void*)(questions), &errMessage);
	check(res, errMessage);

	return *questions;
}
