#include "Question.h"

Question::Question(std::string question, std::vector<std::string> possibleAnswers, unsigned int currectAnswerIndex)
	:m_question(question), m_possibleAnswers(possibleAnswers), m_correctAnswerIndex(currectAnswerIndex)
{
}

std::string Question::getQuestion()
{
	return m_question;
}

std::vector<std::string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}

std::string Question::getCorrectAnswer()
{
	return m_possibleAnswers[m_correctAnswerIndex];
}

unsigned int Question::getCorrectAnswerIndex()
{
	return m_correctAnswerIndex;
}

