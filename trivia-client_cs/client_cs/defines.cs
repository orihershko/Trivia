﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client_cs
{
    public class Defines
    {

        static public int OPTION1 = 0;
        static public int OPTION2 = 1;
        static public int OPTION3 = 2;
        static public int OPTION4 = 3;

        static public int START = 1;

        static public string MESSAGE = "message";
        static public string STATUS = "status";
        static public string USERNAMES = "usernames";
        static public string HIGHSCORE = "highscore";
        static public string NUMBER_OF_GAME = "num_of_game";
        static public string WINS = "wins";
        static public string LOSES = "loses";
        static public string AVG_TIME = "avg_time";
        static public string ID = "id";
        static public string NAME = "name";
        static public string MAX_PLAYERS = "maxPlayers";
        static public string TIME_PER_QUESTION = "timePerQuestion";
        static public string IS_ACTIVE = "isActive";
        static public string USERNAME = "username";
        static public string PASSWORD = "password";
        static public string EMAIL = "email";
        static public string ROOM_ID = "roomId";
        static public string ROOM_NAME = "roomName";
        static public string MAX_USERS = "maxUsers";
        static public string QUESTION_COUNT = "questionCount";
        static public string ANSWER_TIMEOUT = "answerTimeout";
        
         //response defines
        static public byte LOGIN_R = 100;
        static public byte SIGNUP_R = 101;
        static public byte ERROR_R = 102;
        static public byte LOGOUT_R = 103;
        static public byte GET_ROOM_R = 104;
        static public byte GET_PLAYERS_IN_ROOM_R = 105;
        static public byte HIGHSCORE_R = 106;
        static public byte JOIN_ROOM_R = 107;
        static public byte CREATE_ROOM_R = 108;
        static public byte SIGNOUT_R = 109;
        //Admin:
        static public byte CLOSE_ROOM_R = 110;
        static public byte START_GAME_R = 111;
        static public byte GET_ROOM_STATE_R = 112;
        //Member:
        static public byte LEAVE_ROOM_R = 113;
        //game resonse defines:
        static public byte GET_QUESTION_R = 114;
        static public byte SUBMIT_ANSWER_R = 115;
        static public byte GET_GAME_RESULTS_R = 116;
        static public byte PLAYERS_RESULTS_R = 117;
        static public byte LEAVE_GAME_R = 118;
        static public byte GET_GAME_RESULT_R = 119;

        //login request defines
        static public byte SUCCESS = 1;
        static public byte LOGIN = 100;
        static public byte SIGNUP = 101;

       //menu request defines
        static public byte GET_PLAYERS_IN_ROOM = 102;
        static public byte JOIN_ROOM = 103;
        static public byte CREATE_ROOM = 104;
        static public byte HIGHSCORE_ID = 105;
        static public byte GET_ROOMS = 106;
        static public byte SIGNOUT = 107;
        // Admin room request defines:
        static public byte CLOSE_ROOM = 108;
        static public byte START_GAME = 109;
        static public byte GET_ROOM_STATE = 110;

        //room request defines:
        static public byte LEAVE_ROOM = 111;
        //game request defines:
        static public byte GET_QUESTION = 112;
        static public byte SUBMIT_ANSWER = 113;
        static public byte GET_GAME_RESULTS = 114;
        static public byte PLAYERS_RESULTS = 115;
        static public byte LEAVE_GAME = 116;
        static public byte GET_GAME_RESULT = 117;

     
        public const int JOIN_ROOM_MENU = 0;
        public const int CREATE_ROOM_MENU = 1;
        public const int MY_STATUS_MENU = 2;
        public const int HIGHSCORE_MENU = 3;
        public const int SIGNOUT_MENU = 4;
        public const int EXIT_MENU = 5;

        static public int MAX_HALL_OF_FAME = 3;
    }
}
