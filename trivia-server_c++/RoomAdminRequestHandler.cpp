#include "RoomAdminRequestHandler.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(LoggedUser& user, RoomManager& roomManager, Room& room, RequestHandlerFactory *handlerFacroty)
	:m_handlerFacroty(handlerFacroty), m_roomManager(roomManager), m_user(user), m_room(room)
{
}

RoomAdminRequestHandler::~RoomAdminRequestHandler()
{
}

bool RoomAdminRequestHandler::isRequestRelevant(Request request)
{
	if (request.id == START_GAME || request.id == GET_ROOM_STATE || request.id == CLOSE_ROOM)
		return true;

	return false;
}

RequestResult RoomAdminRequestHandler::handleRequest(Request request)
{
	

	if (request.id == START_GAME)
		return startGame(request);

	else if (request.id == GET_ROOM_STATE)
		return getRoomState(request);

	else if (request.id == CLOSE_ROOM)
		return closeRoom(request);

	RequestResult requestResult;
	ErrorResponse res;
	res.message = UNKNOWN_REQUEST_ID_ERROR;
	requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);

	return requestResult;
}

RequestResult RoomAdminRequestHandler::closeRoom(Request request)
{
	RequestResult requestResult;

	try
	{
		CloseRoomResponse res;
		
		m_roomManager.deleteRoom(m_room.getRoomData().id); // Delete current room
		res.status = SUCCESS;

		requestResult.response = JsonResponsePaketSerializer::serializeCloseRoomResponse(res);
		requestResult.newHandler = m_handlerFacroty->createMenuRequestHandler(m_user);
	}
	catch (const std::exception& e)
	{
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
		requestResult.newHandler = this;
	}

	return requestResult;
}

RequestResult RoomAdminRequestHandler::startGame(Request request)
{
	RequestResult requestResult;

	try
	{
		StartGameResponse res;

		m_room.startGame(); // Set "isActive: to be true
		res.status = SUCCESS;

		requestResult.response = JsonResponsePaketSerializer::serializeStartGameResponse(res);
		requestResult.newHandler = m_handlerFacroty->createGameRequestHandler(m_user, m_room);
		requestResult.gameStarted = true;
		requestResult.room = &m_room;

	}
	catch (const std::exception& e)
	{
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
		requestResult.newHandler = this;
	}

	return requestResult;
}

RequestResult RoomAdminRequestHandler::getRoomState(Request request)
{
	RequestResult requestResult;

	try
	{
		GetRoomStateResponse res;

		res.status = SUCCESS;
		res.hasGameBegun = m_room.getRoomData().isActive;
		res.players = m_room.getAllUsers();
		res.answerTimeout = m_room.getRoomData().timePerQuestion;
		res.questionCount = m_room.getRoomData().questionCount;

		requestResult.response = JsonResponsePaketSerializer::serializeGetRoomStateResponse(res);
	}
	catch (const std::exception& e)
	{
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}

	requestResult.newHandler = this;
	return requestResult;
}
