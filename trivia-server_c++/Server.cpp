#include "Server.h"


Server::Server() : _database(new SQLDataBase()), _handlerFactory(new LoginManager(_database), new RoomManager(), new HighScoreTable(_database), new GameManager(_database)), _communicator(&_handlerFactory)
{

}

Server::~Server()
{
	delete _database;
	
}


void Server::run()
{
	_communicator.bindAndListen();
}