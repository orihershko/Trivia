#include "Game.h"

Game::Game(std::vector<Question> questions, std::map<LoggedUser, GameData> players)
	:m_questions(questions), m_players(players), m_question(questions[0])
{
}

Game::~Game()
{
}

Question Game::getQuestionForUser(LoggedUser user)
{
	for (auto player : m_players)
		if (player.first == user)
			return *player.second.currentQuestion;

	throw std::exception("No such user");
}

void Game::submitAnswer(LoggedUser user, unsigned int answerIndex)
{
	for (auto player : m_players) // search after the player
	{
		if (player.first == user)
		{
			Question currentQuestion = getQuestionForUser(user); // current question
			std::string playerAnswer = currentQuestion.getPossibleAnswers()[answerIndex]; // player answer
			
			/* If the player is right - add to his correct counter, if not - add to his worng counter: */
			if (currentQuestion.getCorrectAnswer() == playerAnswer)
				player.second.correctAnswerCount++;
			else
				player.second.wrongAnswerCount++;
			
			return;
		}
	}

	throw std::exception("Bad question/answer/user");
}

void Game::removePlayer(LoggedUser user)
{
	m_players.erase(user);
}

void Game::addPlayer(LoggedUser user)
{
	GameData gameData;
	gameData.currentQuestion = nullptr;
	gameData.correctAnswerCount = 0;
	gameData.averangeAnswerTime = 0;
	gameData.wrongAnswerCount = 0;

	m_players[user] = gameData; // add the new player
}

std::map<LoggedUser, GameData> Game::getPlayers()
{
	return m_players;
}

Question Game::getCurrentQuestion()
{
	m_question = m_questions[m_counter];
	m_counter++;

	return m_question;
}

