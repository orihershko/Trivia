#pragma once
#include "RequestHandlerFactory.h"
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "GameManager.h"

class RequestHandlerFactory;

class GameRequestHandler :public IRequestHandler
{
public:
	GameRequestHandler(LoggedUser user, Game& game, GameManager& gameManager, RequestHandlerFactory * handlerFactory);
	~GameRequestHandler();
	bool isRequestRelevant(Request request);
	RequestResult handleRequest(Request request);
	RequestResult getQuestion(Request request);
	RequestResult submitAnswer(Request request);
	RequestResult getGameResults(Request request);
	RequestResult leaveGame(Request request);

private:
	Game m_game;
	LoggedUser m_user;
	GameManager m_gameManager;
	RequestHandlerFactory *m_handlerFactory;


};

