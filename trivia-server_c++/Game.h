#pragma once
#include "Question.h"
#include "LoggedUser.h"
#include "GameData.h"
#include <map>

class Game
{
public:
	Game(std::vector<Question> questions, std::map<LoggedUser, GameData> players);
	~Game();

	Question getQuestionForUser(LoggedUser user);
	/*
	Function will check if the answer is worng or not,
	and then dd it to the game data of the user
	*/
	void submitAnswer(LoggedUser user, unsigned int answerIndex);
	void removePlayer(LoggedUser user);
	void addPlayer(LoggedUser user);

	std::map<LoggedUser, GameData> getPlayers();
	Question getCurrentQuestion();

private:
	std::vector<Question> m_questions;
	std::map<LoggedUser, GameData> m_players;
	Question m_question;
	unsigned int m_counter = 0;
};


