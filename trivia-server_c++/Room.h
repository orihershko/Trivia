#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "RoomData.h"
#include "LoggedUser.h"

class Room
{
public:
	Room(RoomData metadeta);
	~Room() = default;

	void addUser(LoggedUser loggedUser);
	void removeUser(LoggedUser loggedUser);
	void startGame();

	std::vector<std::string> getAllUsers();
	RoomData getRoomData();

private:
	RoomData m_metadeta;
	std::vector<LoggedUser> m_users;
};
