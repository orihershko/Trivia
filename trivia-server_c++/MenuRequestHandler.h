﻿#pragma once
#include "RoomManager.h"
#include "HighScoreTable.h"
#include "RequestHandlerFactory.h"
#include "Request.h"
#include "‏‏Requests.h"
#include "defines.h"
#include "IRequestHandler.h"
#include <iostream>



class LoggedUser;
class RoomManager;
class HighScoreTable;
class RequestHandlerFactory;
struct Request;
class Requests;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(LoggedUser &user, RoomManager roomManger, HighScoreTable highscoreTable, RequestHandlerFactory*handlerFactor);
	~MenuRequestHandler();

	bool isRequestRelevant(Request request);
	RequestResult handleRequest(Request request);

	RequestResult signout(Request request);
	RequestResult getRooms(Request request);
	//RequestResult getPlayersInRoom(Request request);
	RequestResult getHighscores(Request request);
	RequestResult joinRoom(Request request);
	RequestResult createRoom(Request request);
private:
	LoggedUser m_user;
	RoomManager m_roomManager;
	HighScoreTable m_highscoreTable;
	RequestHandlerFactory *m_handlerFacroty;
};

