#include "HighScoreTable.h"

HighScoreTable::HighScoreTable(IDataBase *database) : m_database(database)
{
}

std::vector<Highscore> HighScoreTable::getHighScores()
{
	return m_database->getHighScores();
}