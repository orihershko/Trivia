﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace client_cs
{
    /// <summary>
    /// Interaction logic for JoinRoom.xaml
    /// </summary>
    public partial class JoinRoom : Window
    {
        Client _client;
        List<RoomData> _rooms;
        public JoinRoom(Client c)
        {
            
            _client = c;
            InitializeComponent();
            _rooms = _client.GetRooms();
            try
            {
                
                for (int i = 0; i < _rooms.Count; i++)
                    listBox.Items.Add(_rooms[i].name);
            }
            catch(Exception)
            {
                
            }
            
           
        }

        private void ButBack_Click(object sender, RoutedEventArgs e)
        {
            Menu menu = new Menu(_client);
            MediaPlayer Sound1 = new MediaPlayer();
            Sound1.Open(new Uri(@"C:\Users\magshimim\source\repos\client_cs\client_cs\bin\Debug\sounds\tick.wav"));
            Sound1.Play();

            this.Close();
            menu.Show();
            Close();
        }

        private void ButRef_Click(object sender, RoutedEventArgs e)
        {
          
            List<RoomData> rooms = _client.GetRooms();
            try
            {
                for (int i = 0; i < rooms.Count; i++)
                    listBox.Items.Add(rooms[i].name);
            }
            catch(Exception)
            {
                

            }
           
        }

        private void ButJoin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string nameRoom = listBox.SelectedItem.ToString();
                for (int i = 0; i < _rooms.Count; i++)
                {
                    if (_rooms[i].name == nameRoom)
                    {

                        _client.GetJoinRoom(_rooms[i].id);
                        break;
                    }
                }
            }
            catch(Exception)
            {
               
            }

            
        }
    }
}
