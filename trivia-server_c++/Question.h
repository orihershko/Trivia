#pragma once
#include <iostream>
#include <string>
#include <vector>
#include<mutex>
class Question
{
public:
	Question(std::string question, std::vector<std::string> possibleAnswers, unsigned int correctAnswerIndex);
	~Question() = default;

	std::string getQuestion();
	std::string getCorrectAnswer();
	unsigned int getCorrectAnswerIndex();
	std::vector<std::string> getPossibleAnswers();
	
private:
	std::string m_question;
	std::vector<std::string> m_possibleAnswers;
	unsigned int m_correctAnswerIndex;
};