﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace client_cs
{
    /// <summary>
    /// Interaction logic for MyStatus.xaml
    /// </summary>
    public partial class MyStatus : Window
    {
        Client _client;
        public MyStatus(Client c)
        {
            _client = c;
            InitializeComponent();
            Highscore highscore = _client.MyStatus();
            TxtAverageTime.Content += highscore.avg_time.ToString();
            TxtNumberOfGames.Content += highscore.num_of_game.ToString();
            TxtNumberOfRight.Content += highscore.wins.ToString();
            TxtNumberOfWrong.Content += highscore.loses.ToString();
           
            

        }

        private void ButBack_Click(object sender, RoutedEventArgs e)
        {
            Menu menu = new Menu(_client);
            MediaPlayer Sound1 = new MediaPlayer();
            Sound1.Open(new Uri(@"C:\Users\magshimim\source\repos\client_cs\client_cs\bin\Debug\sounds\tick.wav"));
            Sound1.Play();
            menu.Show();
            Close();
        }
    }
}
