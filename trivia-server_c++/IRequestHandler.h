#pragma once
#include "Request.h"
#include "RequestResult.h"
struct RequestResult;

class IRequestHandler
{
public:
	virtual ~IRequestHandler() = default;

	virtual bool isRequestRelevant(Request request) = 0;
	virtual RequestResult handleRequest(Request request) = 0;
};