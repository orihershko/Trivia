#include "LoggedUser.h"



LoggedUser::LoggedUser(std::string username) : _username(username)
{

}


LoggedUser::~LoggedUser()
{
	
}


std::string LoggedUser::getUsername() const
{
	return _username;
}

bool LoggedUser::operator==(const LoggedUser & loggedUser) const
{
	return loggedUser.getUsername() == getUsername();
}

bool LoggedUser::operator<(const LoggedUser & loggedUser) const
{
	return loggedUser.getUsername() < getUsername();
}

bool LoggedUser::operator>(const LoggedUser & loggedUser) const
{
	return loggedUser.getUsername() > getUsername();
}
