#include "RoomManager.h"

RoomManager::~RoomManager()
{
}

RoomManager::RoomManager(const RoomManager & other)
{
	m_rooms = other.m_rooms;
}

Room* RoomManager::createRoom(RoomData roomData)
{
	int current_id = 0;
	Room* room;
	std::unique_lock<std::mutex> lck(m_mtx);
	if (m_rooms.size() > 0)
		current_id = (--m_rooms.end())->second->getRoomData().id + 1;

	roomData.id = current_id;
	room = new Room(roomData);
	m_rooms.insert(std::pair<int, Room*>(current_id, room));
	lck.unlock();
	return room;
}


void RoomManager::deleteRoom(int ID)
{
	std::unique_lock<std::mutex> lck(m_mtx);
	for (auto i : m_rooms)
	{
		if (i.second->getRoomData().id == ID)
		{
			m_rooms.erase(i.first);
			lck.unlock();
			return;
		}
	}
	


	throw std::exception("Room not exists");
}


unsigned int RoomManager::getRoomState(int ID)
{
	std::unique_lock<std::mutex> lck(m_mtx);
	for (auto i : m_rooms)
	{
		if (i.first == ID)
		{
			unsigned int active = i.second->getRoomData().isActive;
			lck.unlock();
			return active;
		}
		
	}
		
	
	throw std::exception("Room not exists");
}


std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> roomsData;
	std::unique_lock<std::mutex> lck(m_mtx);
	for (auto i : m_rooms)
		roomsData.push_back(i.second->getRoomData());
	lck.unlock();
	return roomsData;
}


std::vector<std::string> RoomManager::getPlayersInRoom(int ID)
{
	std::unique_lock<std::mutex> lck(m_mtx);
	for (auto i : m_rooms)
	{
		if (i.first == ID)
		{
			std::vector<std::string> users = i.second->getAllUsers();
			lck.unlock();
			return users;
		}
	}
		
			
	
	throw std::exception("Room not exists");
}

Room& RoomManager::getRoom(unsigned int roomId)
{
	std::unique_lock<std::mutex> lck(m_mtx);
	for (auto i : m_rooms)
	{
		if (i.first == roomId)
		{
			lck.unlock();
			return *(i.second);
		}
	}
	
			
	
	throw std::exception("Room not exists");
}


void RoomManager::signout_user(LoggedUser user)
{
	std::unique_lock<std::mutex> lck(m_mtx);
	for (auto i : m_rooms)
	{
		for (auto j : i.second->getAllUsers())
		{
			if (j == user.getUsername())
			{
				i.second->removeUser(user);
				lck.unlock();
				return;
			}

		}
	}
	
	throw std::exception("User not exists in any room");
}

Room* RoomManager::signin_user(LoggedUser user, unsigned int roomId)
{
	Room& room = getRoom(roomId);
	room.addUser(user);

	return &room;
}