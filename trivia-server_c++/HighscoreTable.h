#pragma once
#include <iostream>
#include <vector>
#include "Highscore.h"
#include "IDataBase.h"

class HighScoreTable
{
public:
	HighScoreTable(IDataBase *database);
	~HighScoreTable() = default;

	std::vector<Highscore> getHighScores();

private:
	IDataBase *m_database;
};
