﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace client_cs
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        Client _client = new Client();
       
        public Login()
        {
            InitializeComponent();
            
            
        }
        private void LoginI_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {


            
            _client.GetLogin(TxtUsername.Text, PasswordBox.Password);
            MediaPlayer Sound1 = new MediaPlayer();
            Sound1.Open(new Uri(@"C:\Users\magshimim\source\repos\client_cs\client_cs\bin\Debug\sounds\tick.wav"));
            Sound1.Play();
            Menu m = new Menu(_client);
            m.Show();
            Close();



        }

        private void SignUp_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {


           
            _client.Signup(TxtUsername.Text, PasswordBox.Password, TxtEmail.Text);
             MediaPlayer Sound1 = new MediaPlayer();
             Sound1.Open(new Uri(@"C:\Users\magshimim\source\repos\client_cs\client_cs\bin\Debug\sounds\tick.wav"));
             Sound1.Play();
            Menu m = new Menu(_client);
            m.Show();
            Close();



        }

        private void Exit_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
    }
}
