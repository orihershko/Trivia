#include "MenuRequestHandler.h"




MenuRequestHandler::MenuRequestHandler(LoggedUser & user, RoomManager roomManager, HighScoreTable highscoreTable, RequestHandlerFactory * handlerFactory)
	:m_user(user), m_roomManager(roomManager), m_highscoreTable(highscoreTable), m_handlerFacroty(handlerFactory)
{
}

MenuRequestHandler::~MenuRequestHandler()
{
}


bool MenuRequestHandler::isRequestRelevant(Request request)
{
	if (request.id == GET_PLAYERS_IN_ROOM || request.id == JOIN_ROOM || request.id == CREATE_ROOM || request.id == HIGHSCORE_ID || request.id == GET_ROOMS)
		return true;

	return false;
}


RequestResult MenuRequestHandler::handleRequest(Request request)
{
	RequestResult requestResult;
	ErrorResponse res;

	/*if (request.id == GET_PLAYERS_IN_ROOM)
		return getPlayersInRoom(request);
		*/
	if (request.id == JOIN_ROOM)
		return joinRoom(request);

	else if (request.id == CREATE_ROOM)
		return createRoom(request);

	else if (request.id == HIGHSCORE_ID)
		return getHighscores(request);

	else if (request.id == GET_ROOMS)
		return getRooms(request);

	else if (request.id == SIGNOUT)
		return signout(request);


	res.message = UNKNOWN_REQUEST_ID_ERROR;
	requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);

	return requestResult;
}

RequestResult MenuRequestHandler::signout(Request request)
{
	RequestResult requestResult;
	SignoutRequest userRequest = JsonRequestPacketDeserializer::deSerializeSignoutRequest(request.buffer);

	try
	{
		SignoutResponse res;
		LoggedUser user(m_user);
		m_roomManager.signout_user(user);
		res.status = SUCCESS;
		requestResult.response = JsonResponsePaketSerializer::serializeSignoutResponse(res);
	}
	catch (const std::exception& e)
	{
		
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}

	requestResult.newHandler = this;
	return requestResult;
}

RequestResult MenuRequestHandler::getRooms(Request request)
{
	RequestResult requestResult;

	try
	{
		GetRoomResponse res;
		res.rooms = m_roomManager.getRooms();
		res.status = SUCCESS;
		requestResult.response = JsonResponsePaketSerializer::serializeGetRoomResponse(res);
	}
	catch (const std::exception& e)
	{
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}

	requestResult.newHandler = this;
	return requestResult;
}

/*RequestResult MenuRequestHandler::getPlayersInRoom(Request request)
{
	RequestResult requestResult;
	GetPlayersInRoomRequest userRequest = JsonRequestPacketDeserializer::deSerializeGetPlayersInRoomRequest(request.buffer);

	try
	{
		GetPlayersInRoomResponse res;
		res.usernames = m_roomManager.getPlayersInRoom(userRequest.roomId);
		requestResult.response = JsonResponsePaketSerializer::serializeResponse(res);
	}
	catch (const std::exception& e)
	{
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}

	requestResult.newHandler = this;
	return requestResult;
}*/

RequestResult MenuRequestHandler::getHighscores(Request request)
{
	RequestResult requestResult;
	GetHighscoreRequest userRequest = JsonRequestPacketDeserializer::deSerializeGetHighscoreRequest(request.buffer);
	try
	{
		HighscoreResponse res;
		res.status = SUCCESS;
		res.highscore = m_highscoreTable.getHighScores();
		requestResult.response = JsonResponsePaketSerializer::serializeHighscoreResponse(res);
	}
	catch (const std::exception& e)
	{
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}

	requestResult.newHandler = this;
	return requestResult;
}

RequestResult MenuRequestHandler::joinRoom(Request request)
{
	RequestResult requestResult;
	JoinRoomRequest userRequest = JsonRequestPacketDeserializer::deSerializeJoinRoomRequest(request.buffer);

	try
	{
		JoinRoomResponse res;
		Room *room;

		res.status = SUCCESS;

		room = m_roomManager.signin_user(m_user, userRequest.roomId);
		RoomData data = room->getRoomData();
		if (data.maxPlayers == room->getAllUsers().size() || data.isActive)
		{
			throw std::exception("Room is full or is active!");
		}
		requestResult.newHandler = m_handlerFacroty->createRoomMemberRequestHandler(m_user, *room, m_roomManager);
		requestResult.response = JsonResponsePaketSerializer::serializeJoinRoomResponse(res);
	}
	catch (const std::exception& e)
	{
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
		requestResult.newHandler = this;
	}

	return requestResult;
}

RequestResult MenuRequestHandler::createRoom(Request request)
{
	RequestResult requestResult;
	CreateRoomRequest userRequest = JsonRequestPacketDeserializer::deSerializeCreateRoomRequest(request.buffer);

	try
	{
		CreateRoomResponse res;
		RoomData data;
		Room *room;

		data.timePerQuestion = userRequest.answerTimeout;
		data.isActive = false; // The room wont be ative untill the admin will start the game.
		data.maxPlayers = userRequest.maxUsers;
		data.name = userRequest.roomName;
		data.questionCount = userRequest.questionCount;

		res.status = SUCCESS;

		room = m_roomManager.createRoom(data);
		m_roomManager.signin_user(m_user, room->getRoomData().id);
		requestResult.newHandler = m_handlerFacroty->createRoomAdminRequestHandler(m_user, *room, m_roomManager);
		requestResult.response = JsonResponsePaketSerializer::serializeCreateRoomResponse(res);
	}
	catch (const std::exception& e)
	{
		ErrorResponse res;
		res.message = e.what();
		requestResult.newHandler = this;
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}


	return requestResult;
}
