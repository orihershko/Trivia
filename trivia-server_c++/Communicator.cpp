#include "Communicator.h"

Communicator::Communicator(RequestHandlerFactory *handlerFacroty) : _handlerFactory(handlerFacroty)
{
	_port = 1727;
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
	{
		cout << "Error - INVALID SOCKET\n";
		system("PAUSE");
	}
}


Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);
	}
	catch (...) {}
}

void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(_port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

										// again stepping out to the global namespace
										// Connects between the socket and the configuration (port and etc..)
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << _port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		handleRequests();
	}
}


void initRequest(struct Request *request, int type, string data)
{
	request->buffer = Buffer(data.begin(), data.end());
	request->id = type;
	time(&request->receivalTime);
}


void Communicator::handleRequests()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	cout << "Client accepted. Server and client can speak" << endl;

	try
	{
		_clients[client_socket] = _handlerFactory->createLoginRequestHandler();
		std::thread clientThread(&Communicator::startThreadForNewClient, this, client_socket);
		clientThread.detach();
	}
	catch (...) {}
}


void Communicator::startThreadForNewClient(SOCKET clientSocket)
{
	while (true)
	{
		try
		{
			struct Request request;
			request.id = Helper::getMessageTypeCode(clientSocket);
			int length = Helper::getIntPartFromSocket(clientSocket);
			request.buffer = Helper::getStringPartFromSocket(clientSocket, length);
			time(&request.receivalTime);
		

			RequestResult requestResult = _clients[clientSocket]->handleRequest(request);
			Helper::sendData(clientSocket, string(requestResult.response.begin(), requestResult.response.end()));
			
			if (_clients[clientSocket] != requestResult.newHandler) // handler have changes - delete the old.
			{

				_clients[clientSocket] = requestResult.newHandler;
			}

			
			
		}
		catch (const std::exception& e)
		{
			UNREFERENCED_PARAMETER(e);
			_clients.erase(clientSocket);
			closesocket(clientSocket);
			break;
		}
	}
}
