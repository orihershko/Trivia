﻿#include "RequestHandlerFactory.h"



RequestHandlerFactory::RequestHandlerFactory(LoginManager *loginManager, RoomManager *roomManager, HighScoreTable *highScoreTable, GameManager *gameManager) : _gameManager(gameManager), _loginManager(loginManager), _roomManager(roomManager), _highScoreTable(highScoreTable)
{
}

RequestHandlerFactory::~RequestHandlerFactory()
{
	delete _loginManager;
	delete _roomManager;
	delete _gameManager;
	delete _highScoreTable;
}


LoginRequestHandler *RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(*_loginManager, this);
}


MenuRequestHandler *RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
	return new MenuRequestHandler(user, *_roomManager, *_highScoreTable, this);
}

RoomAdminRequestHandler *RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, Room room, RoomManager& roomManager)
{
	return new RoomAdminRequestHandler(user, roomManager, room, this);
}

RoomMemberRequestHandler * RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, Room room, RoomManager & roomManager)
{
	return new RoomMemberRequestHandler(user, roomManager, room, this);
}

GameRequestHandler * RequestHandlerFactory::createGameRequestHandler(LoggedUser user, Room room)
{
	return new GameRequestHandler(user, _gameManager->createGame(room), *_gameManager, this);
}
