#pragma once
#include <iostream>
#include <vector>
#include "defines.h"


struct LoginRequest
{
	std::string username;
	std::string password;
};

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};
struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};

struct JoinRoomRequest
{
	unsigned int roomId;
};

struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct GetRoomsRequest
{
	unsigned int status;
};

struct GetHighscoreRequest
{
	unsigned int status;
};

struct SignoutRequest
{
	std::string username;
};

struct SubmitAnswerRequest
{
	unsigned int answerId;
};

struct GetQuestionRequest
{
	unsigned int status;
};

struct LeaveGameRequest
{
	unsigned int status;
};

struct GetGameResultRequest
{
	unsigned int status;
};

struct JsonRequestPacketDeserializer
{
	static LoginRequest deserializeLoginRequest(Buffer buffer)
	{
		std::vector<uint8_t> contests;
		contests.insert(contests.begin(), buffer.begin(), buffer.end());
		json j = cbor(contests);
		LoginRequest req;
		req.password = j.at(std::string(PASSWORD)).get<std::string>();
		req.username = j.at(std::string(USERNAME)).get<std::string>();
		return req;
	}

	static SignupRequest deserializeSignupRequest(Buffer buffer)
	{
		std::vector<uint8_t> contests;
		contests.insert(contests.begin(), buffer.begin(), buffer.end());
		
		json j = cbor(contests);
		SignupRequest req;
		req.password = j.at(std::string(PASSWORD)).get<std::string>();
		req.username = j.at(std::string(USERNAME)).get<std::string>();
		req.email = j.at(std::string(EMAIL)).get<std::string>();
		return req;
	}

	static GetPlayersInRoomRequest deSerializeGetPlayersInRoomRequest(Buffer buffer)
	{
		std::vector<uint8_t> contests;
		contests.insert(contests.begin(), buffer.begin(), buffer.end());
		json j = cbor(contests);
		GetPlayersInRoomRequest req;
		req.roomId = j.at(std::string(ROOM_ID)).get<unsigned int>();
		return req;
	}

	static JoinRoomRequest deSerializeJoinRoomRequest(Buffer buffer)
	{
		std::vector<uint8_t> contests;
		contests.insert(contests.begin(), buffer.begin(), buffer.end());
		json j = cbor(contests);
		JoinRoomRequest req;
		req.roomId = j.at(std::string(ROOM_ID)).get<unsigned int>();
		return req;
	}

	static CreateRoomRequest deSerializeCreateRoomRequest(Buffer buffer)
	{
		std::vector<uint8_t> contests;
		contests.insert(contests.begin(), buffer.begin(), buffer.end());
		json j = cbor(contests);
		CreateRoomRequest req;
		req.roomName = j.at(std::string(ROOM_NAME)).get<std::string>();
		req.maxUsers = j.at(std::string(MAX_USERS)).get<unsigned int>();
		req.questionCount = j.at(std::string(QUESTION_COUNT)).get<unsigned int>();
		req.answerTimeout = j.at(std::string(ANSWER_TIMEOUT)).get<unsigned int>();
		return req;
	}

	static GetRoomsRequest deSerializeGetRoomsRequest(Buffer buffer)
	{
		std::vector<uint8_t> contests;
		contests.insert(contests.begin(), buffer.begin(), buffer.end());
		json j = cbor(contests);
		GetRoomsRequest req;
		req.status = j.at(std::string(STATUS)).get<unsigned int>();
		return req;
	}

	static GetHighscoreRequest deSerializeGetHighscoreRequest(Buffer buffer)
	{
		std::vector<uint8_t> contests;
		contests.insert(contests.begin(), buffer.begin(), buffer.end());
		json j = cbor(contests);
		GetHighscoreRequest req;
		req.status = j.at(std::string(STATUS)).get<unsigned int>();
		return req;
	}

	static SignoutRequest deSerializeSignoutRequest(Buffer buffer)
	{
		std::vector<uint8_t> contests;
		contests.insert(contests.begin(), buffer.begin(), buffer.end());
		json j = cbor(contests);
		SignoutRequest req;
		req.username = j.at(std::string(USERNAME)).get<std::string>();
		return req;
	}

	static SubmitAnswerRequest deSerializeSubmitAnswerRequest(Buffer buffer)
	{
		std::vector<uint8_t> contests;
		contests.insert(contests.begin(), buffer.begin(), buffer.end());
		json j = cbor(contests);
		SubmitAnswerRequest req;
		req.answerId = j.at(std::string(ANSWER_ID)).get<unsigned int>();
		return req;
	}
	static GetQuestionRequest deSerializeGetQuestionRequest(Buffer buffer)
	{
		std::vector<uint8_t> contests;
		contests.insert(contests.begin(), buffer.begin(), buffer.end());
		json j = cbor(contests);
		GetQuestionRequest req;
		req.status = j.at(std::string(STATUS)).get<unsigned int>();
		return req;
	}
	static LeaveGameRequest deSerializeLeaveGameRequest(Buffer buffer)
	{
		std::vector<uint8_t> contests;
		contests.insert(contests.begin(), buffer.begin(), buffer.end());
		json j = cbor(contests);
		LeaveGameRequest req;
		req.status = j.at(std::string(STATUS)).get<unsigned int>();
		return req;
	}
	static GetGameResultRequest deSerializeGetGameResultRequest(Buffer buffer)
	{
		std::vector<uint8_t> contests;
		contests.insert(contests.begin(), buffer.begin(), buffer.end());
		json j = cbor(contests);
		GetGameResultRequest req;
		req.status = j.at(std::string(STATUS)).get<unsigned int>();
		return req;
	}

	static json cbor(Buffer buffer)
	{
		std::string j;
		for (unsigned int i = 0; i < buffer.size(); i++)
		{
			j.push_back((char)buffer[i]);
		}
		return json::parse(j);
	}

};
