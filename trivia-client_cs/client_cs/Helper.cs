﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
namespace client_cs
{
    class Helper
    {
        public int GetId(NetworkStream stream)
        {
            byte[] data = new byte[1];
            stream.Read(data, 0, 1);
            int id = data[0];
            return id;
        }
        public int GetLength(NetworkStream stream)
        {
            byte[] data = new byte[sizeof(int)];
            stream.Read(data, 0, data.Length);
            int length = BitConverter.ToInt32(data, 0);
            return length;
        }
        public byte[] GetMessage(NetworkStream stream,int length)
        {
            byte[] data = new byte[length];
            stream.Read(data, 0, data.Length);
            return data;
        }
        public Request GetRequest(int id,byte[] buffer)
        {
            Request request = new Request { _id = (uint)id, _receivalTime = DateTime.Now, _buffer = buffer };
            return request;
            
        }
    }
}
