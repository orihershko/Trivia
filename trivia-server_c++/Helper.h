#pragma once

#include <string>
#include<vector>
#include <WinSock2.h>
using namespace std;
using Buffer = std::vector<uint8_t>;
class Helper
{
public:

	static int getMessageTypeCode(SOCKET sc);
	static int getIntPartFromSocket(SOCKET sc);
	static Buffer getStringPartFromSocket(SOCKET sc, int bytesNum);
	static void sendData(SOCKET sc, std::string message);
	static void sendUpdateMessageToClient(SOCKET sc, string fileContent, string currUser, string nextUser, int position);
	static string getPaddedNumber(int num, int digits);

private:
	static char* getPartFromSocket(SOCKET sc, int bytesNum);
	static char* getPartFromSocket(SOCKET sc, int bytesNum, int flags);

};