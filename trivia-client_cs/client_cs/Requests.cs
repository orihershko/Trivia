﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace client_cs
{
    class Requests
    {
        
       public struct LoginRequest
        {
            public string username;
            public string password;
        };

        public struct SignupRequest
        {
            public string username;
            public string password;
            public string email;
        };

        public struct GetPlayersInRoomRequest
        {
            public uint roomId;
        };

        public struct JoinRoomRequest
        {
            public uint roomId;
        };

        public struct CreateRoomRequest
        {
            public string roomName;
            public uint maxUsers;
            public uint questionCount;
            public uint answerTimeout;
        };

        public struct HighscoreRequest
        {
            public uint status;
        };

        public struct GetRoomsRequest
        {
            public uint status;
        };

        public struct SignoutRequest
        {
            public uint status;
        };

        public struct GetRoomStateRequest
        {
            public uint status;
        };

        public struct SubmitAnswerRequest
        {
           public uint answerId;
        };

        public struct GetQuestionRequest
        {
            public uint status;
        };

        public struct LeaveGameRequest
        {
            public uint status;
        };

        public struct GetGameResultRequest
        {
            public uint status;
        };

        public struct StartGameRequest
        {
            public uint status;
        };

        public struct JsonRequestPacketSerializer
        {
            public static byte[] SerializeLoginRequest(LoginRequest user)
            {
                byte id = Defines.LOGIN;
                string json = JsonConvert.SerializeObject(user, Formatting.None);
                return MakeMessage(json, id);
            }
            public static byte[] SerializeSignupRequest(SignupRequest user)
            {
                byte id = Defines.SIGNUP;
                string json = JsonConvert.SerializeObject(user, Formatting.None);
                return MakeMessage(json, id);
            }
            public static byte[] SerializeGetPlayersInRoomRequest(GetPlayersInRoomRequest PlayersInRoom)
            {
                byte id = Defines.GET_PLAYERS_IN_ROOM;
                string json = JsonConvert.SerializeObject(PlayersInRoom, Formatting.None);
                return MakeMessage(json, id);
            }
            public static byte[] SerializeJoinRoomRequest(JoinRoomRequest room)
            {
                byte id = Defines.JOIN_ROOM;
                string json = JsonConvert.SerializeObject(room, Formatting.None);
                return MakeMessage(json, id);
            }
            public static byte[] SerializeCreateRoomRequest(CreateRoomRequest room)
            {
                byte id = Defines.CREATE_ROOM;
                string json = JsonConvert.SerializeObject(room, Formatting.None);
                return MakeMessage(json, id);
            }
            public static byte[] SerializeGetRoomsRequest(GetRoomsRequest message)
            {
                byte id = client_cs.Defines.GET_ROOMS;
                string json = JsonConvert.SerializeObject(message, Formatting.None);
                return MakeMessage(json, id);
            }
            public static byte[] SerializeHighscoreRequest(HighscoreRequest message)
            {
                byte id = client_cs.Defines.HIGHSCORE_ID;
                string json = JsonConvert.SerializeObject(message, Formatting.None);
                return MakeMessage(json, id);
            }
            public static byte[] SerializeSignoutRequest(SignoutRequest message)
            {
                byte id = client_cs.Defines.SIGNOUT;
                string json = JsonConvert.SerializeObject(message, Formatting.None);
                return MakeMessage(json, id);
            }

            public static byte[] SerializeGetRoomStateRequest(GetRoomStateRequest message)
            {
                byte id = client_cs.Defines.GET_ROOM_STATE;
                string json = JsonConvert.SerializeObject(message, Formatting.None);
                return MakeMessage(json, id);
            }

            public static byte[] SerializeSubmitAnswerRequest(SubmitAnswerRequest message)
            {
                byte id = client_cs.Defines.SUBMIT_ANSWER;
                string json = JsonConvert.SerializeObject(message, Formatting.None);
                return MakeMessage(json, id);
            }

            public static byte[] SerializeGetQuestionRequest(GetQuestionRequest message)
            {
                byte id = client_cs.Defines.GET_QUESTION;
                string json = JsonConvert.SerializeObject(message, Formatting.None);
                return MakeMessage(json, id);
            }

            public static byte[] SerializeLeaveGameRequest(LeaveGameRequest message)
            {
                byte id = client_cs.Defines.LEAVE_GAME;
                string json = JsonConvert.SerializeObject(message, Formatting.None);
                return MakeMessage(json, id);
            }

            public static byte[] SerializeGetGameResultRequest(GetGameResultRequest message)
            {
                byte id = client_cs.Defines.GET_GAME_RESULT;
                string json = JsonConvert.SerializeObject(message, Formatting.None);
                return MakeMessage(json, id);
            }

            public static byte[] SerializeStartGameRequest(StartGameRequest message)
            {
                byte id = client_cs.Defines.START_GAME;
                string json = JsonConvert.SerializeObject(message, Formatting.None);
                return MakeMessage(json, id);
            }

            public static byte[] MakeMessage(string j, byte id)
            {
                byte[] message = System.Text.Encoding.ASCII.GetBytes(j);
                byte[] buffer = new byte[message.Length + 5];
                buffer[0] = id;
                buffer[1] = (byte)(j.Length >> 24);
                buffer[2] = (byte)(j.Length >> 16);
                buffer[3] = (byte)(j.Length >> 8);
                buffer[4] = (byte)(j.Length);
                for(int i=0;i<message.Length;i++)
                {
                    buffer[i + 5] = message[i];
                }
                return buffer;
            }

        };

    }
}
