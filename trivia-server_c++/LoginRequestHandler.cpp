﻿#include "LoginRequestHandler.h"
#include "‏‏Requests.h"
#include "Responses.h"

LoginRequestHandler::LoginRequestHandler(LoginManager &loginManager, RequestHandlerFactory *handlerFactory) : _loginManager(loginManager), _handlerFactory(handlerFactory)
{
}


LoginRequestHandler::~LoginRequestHandler()
{
	delete _handlerFactory;
}


bool LoginRequestHandler::isRequestRelevant(Request request)
{
	if (request.id == LOGIN || request.id == SIGNUP)
		return true;
	return false;
}


RequestResult LoginRequestHandler::handleRequest(Request request)
{
	if (request.id == LOGIN)
		return login(request);
	if (request.id == SIGNUP)
		return signup(request);
	return signup(request); // 100: signup, 101: sigin.
}


RequestResult LoginRequestHandler::login(Request request)
{
	RequestResult requestResult;
	LoginRequest userRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(request.buffer);

	try
	{
		_loginManager.login(userRequest.username, userRequest.password);
		LoginResponse res;
		res.status = SUCCESS;
		requestResult.newHandler = _handlerFactory->createMenuRequestHandler(LoggedUser(userRequest.username));
		requestResult.response = JsonResponsePaketSerializer::serializeLoginResponse(res);
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}

	
	return requestResult;
}


RequestResult LoginRequestHandler::signup(Request request)
{
	RequestResult requestResult;
	SignupRequest userRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(request.buffer);
	try
	{
		_loginManager.signup(userRequest.username, userRequest.password, userRequest.email);
		SignupResponse res;
		res.status = SUCCESS;
		requestResult.response = JsonResponsePaketSerializer::serializeSignupResponse(res);
		requestResult.newHandler = _handlerFactory->createMenuRequestHandler(LoggedUser(userRequest.username));
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
		ErrorResponse res;
		res.message = e.what();
		requestResult.response = JsonResponsePaketSerializer::serializeErrorResponse(res);
	}


	
	return requestResult;
}
