#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <fstream>
#include <thread>
#include <time.h>
#include <unordered_map>

#include "Helper.h"
#include "IDataBase.h"
#include "SQLDataBase.h"
#include "RequestHandlerFactory.h"

using std::cout;
using std::endl;

class Communicator
{
public:
	Communicator(RequestHandlerFactory *handlerFacroty);
	~Communicator();

	void bindAndListen();
	void handleRequests();
	void startThreadForNewClient(SOCKET clientSocket);

private:
	std::unordered_map<SOCKET, IRequestHandler*> _clients;
	RequestHandlerFactory *_handlerFactory;

	SOCKET _socket;
	unsigned int _port;
};

