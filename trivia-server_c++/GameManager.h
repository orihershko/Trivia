#pragma once
#include "IDataBase.h"
#include "Game.h"
#include "Room.h"
#include<mutex>
class GameManager
{
public:
	GameManager(IDataBase *database);
	GameManager(const GameManager& other);
	~GameManager();

	Game createGame(Room room);
	void deleteGame(Game game);

private:
	IDataBase *m_database;
	std::vector<Game> m_games;
	std::mutex m_mtx;
};

