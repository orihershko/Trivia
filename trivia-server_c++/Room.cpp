#include "Room.h"


Room::Room(RoomData metadeta) : m_metadeta(metadeta)
{
	
}

RoomData Room::getRoomData()
{
	return m_metadeta;
}


void Room::addUser(LoggedUser loggedUser)
{
	if (m_users.size() == 4)
		throw std::exception("Room is full");

	m_users.push_back(loggedUser);
}


void Room::removeUser(LoggedUser loggedUser)
{
	std::vector<LoggedUser>::iterator it;
	
	for (it = m_users.begin(); it != m_users.end(); ++it) // Loop over the users
		if (it->getUsername() == loggedUser.getUsername()) // if user found
			break; // then break now

	if (it != m_users.end())
		m_users.erase(it);
	else
		throw std::exception("User not exists");
}

void Room::startGame()
{
	m_metadeta.isActive = true;
}


std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> users_str;
	for (auto it : m_users)
		users_str.push_back(it.getUsername());

	return users_str;
}