#pragma once
#include <iostream>
#include <time.h>
#include <vector>
using Buffer = std::vector<uint8_t>;

struct Request
{
	unsigned int id;
	time_t receivalTime;
	Buffer buffer;
};