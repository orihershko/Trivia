#pragma once
#include <utility>
#include <string>
#include <vector>
#include <io.h>
#include "IDataBase.h"
#include "sqlite3.h"

using std::cout;
using std::endl;

class SQLDataBase : public IDataBase
{
public:
	SQLDataBase();
	~SQLDataBase();

	bool doesUserExists(std::string username) override;
	void addUser(const std::string& userName, const std::string& password, const std::string& email) override;
	void signIn(const std::string& userName, const std::string& password) override;

	std::vector<Highscore> getHighScores() override;
	std::vector<Question> getQuestions(unsigned int numberOfQuestions) override;

private:
	std::string m_FileName;
	sqlite3* m_db;
	bool open();
	
};

