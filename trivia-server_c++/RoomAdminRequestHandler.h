#pragma once
#include <iostream>
#include "LoginManager.h"
#include "RequestHandlerFactory.h"
#include "RequestResult.h"
#include "Request.h"
#include "Responses.h"
#include "defines.h"
class RequestHandlerFactory;


class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(LoggedUser& user, RoomManager& roomManager, Room& room, RequestHandlerFactory *handlerFacroty);
	~RoomAdminRequestHandler();

	bool isRequestRelevant(Request request) override;
	RequestResult handleRequest(Request request) override;

	RequestResult closeRoom(Request request);
	RequestResult startGame(Request request);
	RequestResult getRoomState(Request request);

private:
	RequestHandlerFactory * m_handlerFacroty;
	RoomManager m_roomManager;
	LoggedUser m_user;
	Room m_room;
};

