﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PeterO.Cbor;
namespace client_cs
{
    public class Responses
    {
      /*  public struct LoginResponse
        {
           public uint _status;
        };

        public struct SignupResponse
        {
            public uint _status;
        };*/

        public struct ErrorResponse
        {
            public string message;
        };

       /* public struct SignoutResponse
        {
            public uint _status;
        };*/
        public struct GetRoomResponse
        {
            public uint status;
            public List<RoomData> rooms;
        };
        public struct GetRoomStateResponse
        {
            public uint status;
            public bool hasGameBegun;
            public List<string> players;
            public uint questionCount;
            public uint answerTimeout;
        };
        public struct HighscoreResponse
        {
            public uint status;
            public List<Highscore> highscore;
        };
        public struct GetQuestionResponse
        {
            public List<AnswerResponse> answers;
            public uint status;
            public string question;
            
        };

        public struct SubmitAnswerResponse
        {
           public uint status;
           public uint correctAnswerId;
        };

        public struct PlayerResults
        {
            public string username;
            public uint correctAnswerCount;
            public uint wrongAnswerCount;
            public uint averagseAnswerTime;
        };

        public struct GetGameResultsResponse
        {
            public uint status;
            public List<PlayerResults> results;
        };

        public struct LeaveGameResponse
        {
            public uint status;
        };

        public struct StartGameResponse
        {
            public uint status;
        };
        public struct AnswerResponse
        {
            public uint option;
            public string answer;

            public int CompareTo(AnswerResponse other)
            {
                if (option > other.option)
                    return 1;
                else if (option < other.option)
                    return  -1;
                return 0;
            }
        };
        

        /*  public struct JoinRoomResponse
          {
              public uint _status;

          };
          public struct CreateRoomResponse
          {
              public uint _status;
          };*/




        public struct JsonResponsePaketDeserializer
        {
            public static GetRoomResponse DeSerializeGetRoomResponse(byte[] buffer)
            {
                var c = CBORObject.DecodeFromBytes(buffer);
                string result = c.ToJSONString();
               
                GetRoomResponse response = JsonConvert.DeserializeObject<GetRoomResponse>(result);
                
                return response;

            }
            public static GetRoomStateResponse DeSerializeGetRoomStateResponse(byte[] buffer)
            {


                var c = CBORObject.DecodeFromBytes(buffer);
                string result = c.ToJSONString();
              
                GetRoomStateResponse response = JsonConvert.DeserializeObject<GetRoomStateResponse>(result);
               
               
                return response;

            }
            public static HighscoreResponse DeSerializeHighscoreResponse(byte[] buffer)
            {

                var c = CBORObject.DecodeFromBytes(buffer);
                string result = c.ToJSONString();
         
                HighscoreResponse response = JsonConvert.DeserializeObject<HighscoreResponse>(result); 
             
                return response;

            }
            public static ErrorResponse DeSerializeErrorResponse(byte[] buffer)
            {
               
                var c = CBORObject.DecodeFromBytes(buffer);
                string result = c.ToJSONString();
                ErrorResponse response = JsonConvert.DeserializeObject<ErrorResponse>(result);
                return response;

            }

            public static GetQuestionResponse DeSerializeGetQuestionResponse(byte[] buffer)
            {
                var c = CBORObject.DecodeFromBytes(buffer);
                string result = c.ToJSONString();
                return JsonConvert.DeserializeObject<GetQuestionResponse>(result);
              
            }

            public static SubmitAnswerResponse DeSerializeSubmitAnswerResponse(byte[] buffer)
            {
                var c = CBORObject.DecodeFromBytes(buffer);
                string result = c.ToJSONString();
                return JsonConvert.DeserializeObject<SubmitAnswerResponse>(result);
            }

            public static GetGameResultsResponse DeSerializeGetGameResultsResponse(byte[] buffer)
            {
                var c = CBORObject.DecodeFromBytes(buffer);
                string result = c.ToJSONString();
                return JsonConvert.DeserializeObject<GetGameResultsResponse>(result);
            }
            public static LeaveGameResponse DeSerializeLeaveGameResponse(byte[] buffer)
            {
                var c = CBORObject.DecodeFromBytes(buffer);
                string result = c.ToJSONString();
                return JsonConvert.DeserializeObject<LeaveGameResponse>(result);
            }
            public static StartGameResponse DeSerializeStartGameResponse(byte[] buffer)
            {
                var c = CBORObject.DecodeFromBytes(buffer);
                string result = c.ToJSONString();
                return JsonConvert.DeserializeObject<StartGameResponse>(result);
            }

        };
    }
}
