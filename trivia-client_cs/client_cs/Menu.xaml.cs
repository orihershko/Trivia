﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;



namespace client_cs
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        Client _client;
        int type = 0;
        public Menu( Client c)
        {
            _client = c;
            InitializeComponent();
            Mouse.OverrideCursor = Cursors.None;
          


        }


        private void Form_KeyDown(object sender, KeyEventArgs e)
        {
           
            if (e.Key == Key.Down)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri(@"pack://siteoforigin:,,,/sounds/tick.wav"));
                Sound1.Play();
                Thickness margin = point.Margin;
                if (margin.Bottom > 30)
                {
                    margin.Bottom -= 60;
                    type++;

                }
                else
                {
                    margin.Bottom = 320;
                    type = Defines.JOIN_ROOM_MENU;
                }

                point.Margin = margin;
            }
            if (e.Key == Key.Up)
            {
                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri(@"pack://siteoforigin:,,,/sounds/tick.wav"));
                Sound1.Play();
                Thickness margin = point.Margin;
                if (margin.Bottom < 320)
                {
                    margin.Bottom += 60;
                    type--;

                }
                else
                {
                    margin.Bottom = 20;
                    type = Defines.EXIT_MENU;
                }
                point.Margin = margin;
            }
             if (e.Key == Key.Enter || e.Key == Key.Space || e.Key == Key.RightShift)
            {

                MediaPlayer Sound1 = new MediaPlayer();
                Sound1.Open(new Uri(@"pack://siteoforigin:,,,/sounds/tick.wav"));
                Sound1.Play();
                Mouse.OverrideCursor = Cursors.Arrow;
                switch (type)
                {
                    case Defines.JOIN_ROOM_MENU:
                        {
                            JoinRoom joinRoom = new JoinRoom(_client);
                            joinRoom.ShowDialog();
                            Close();
                            break;

                        }
                    case Defines.CREATE_ROOM_MENU:
                        {
                            CreateRoom createRoom = new CreateRoom(_client);
                            createRoom.ShowDialog();
                            Close();
                            break;
                        }
                    case Defines.MY_STATUS_MENU:
                        {
                            MyStatus status = new MyStatus(_client);
                            status.ShowDialog();
                            Close();
                            
                            break;
                        }
                    case Defines.HIGHSCORE_MENU:
                        {
                            HighscoreWindow window = new HighscoreWindow(_client);
                            window.ShowDialog();
                            Close();
                            break;
                        }
                    case Defines.SIGNOUT_MENU:
                        {
                            _client.Exit();
                            Login mainWindow = new Login();
                            mainWindow.ShowDialog();
                            Close();
                            break;
                        }
                    case Defines.EXIT_MENU:
                        {
                            _client.Exit();
                            Close();
                            break;
                        }
                }


            }
        }


    }
}
